<?php include("includes/php/restrito.php") ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php include("includes/header.php");


        ?>


<?php
        
    //Remove locações não finalizadas
    $query_marcados = mysqli_query($con,"SELECT * FROM locacao WHERE status = 'marcado'");
        $total = mysqli_num_rows($query_marcados);
        
        if($total == 0){

            echo '<script >window.location = "index_admin.php";</script>' ; 
 

        }
        
        ?>




    </head>
    <body>
        <div id="wrapper">
            <?php include("includes/topo.php") ?>
            <!-- /. NAV TOP  -->
            <?php include("includes/navbar.php") ?>
            <div id="page-wrapper" >
                <div id="page-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Lista de produtos</h2>   

                        </div>
                    </div>
                    <!-- /. ROW  -->

                    <div class="row">
                        <!--- Formulário --------------------------------------------------------->
                        <?php
    $tipo = "produto";

            $titl = "Inserir produtos";

            $tel01 = null;
            $tel02 = null;
            $tel03 = null;


            $query = mysqli_query($con,"SELECT * FROM locacao WHERE status = 'marcado' ");



            $linha=mysqli_fetch_array($query);

            $id_locacao = $linha['id'];
            $nome_cliente = $linha['nome'];
            $endereco = $linha['endereco'];
            $telefones = $linha['telefones'];
            
            $fim = $linha['fim'];
            $dias = $linha['dias'];
            $status = $linha['status'];

            if(!empty($telefones)){

                $tel = explode('|', $telefones);

                $tel01 = $tel[0];
                $tel02 = $tel[1];
                $tel03 = $tel[2];

            }






                        ?>
                        <!---/Formulário --------------------------------------------------------->

                        <!--- Tabela --------------------------------------------------------->
                        <div class="col-md-6" id="tabela">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-file-text-o" aria-hidden="true"></i> Itens
                                </div>
                                <div class="panel-body">



                                    <div class="table-responsive">
                                        <table class="table table-bordered" >
                                            <thead>
                                                <tr>
                                                    <th >Nome</th>
                                                    <th style="width: 90px">Qte</th>                                                    
                                                    <th style="width: 90px">Preço</th>                                                    

                                                    <th  style="width: 80px"></th>

                                                </tr>

                                            </thead>
                                            <tbody>

                                                <?php

                                                $diaria = "0";                              

                                                $query_lista = mysqli_query($con,"SELECT * FROM lista WHERE id_locacao = '$id_locacao' ");    
                                                while($linha_lista=mysqli_fetch_array($query_lista)){

                                                    $id_item = $linha_lista['id_produto'];
                                                    $qte_item = $linha_lista['qte'];



                                                    $query_produto_lista = mysqli_query($con,"SELECT * FROM produtos WHERE id = '$id_item' ");
                                                    $linha_produto=mysqli_fetch_array($query_produto_lista);

                                                    $id_item01 = $linha_produto['id'];
                                                    $nome = $linha_produto['nome'];
                                                    $grupo = $linha_produto['grupo'];
                                                    $tamanhos = $linha_produto['tamanhos'];
                                                    $preco = $linha_produto['preco'];

                                                    if($grupo != "0"){

                                                        $query_grupo = mysqli_query($con,"SELECT * FROM grupos WHERE id = '$grupo' ");   
                                                        $linha_grupo=mysqli_fetch_array($query_grupo);

                                                        $preco = $linha_grupo['preco'];

                                                        $nome = "$nome - nº $tamanhos";
                                                    }


                                                    $diaria += $preco;


                                                ?>

                                                <tr class="">
                                                    <td><?php echo "$nome" ?></td>
                                                    <td><?php echo "$qte_item" ?></td>
                                                    <td>R$ <?php echo number_format($preco, 2, ',', '.'); ?></td>


                                                    <td>

                                                        <a href="scriptz/deletar.php?id=<?php echo "$id_item01" ?>&tipo=item_na_lista" class="btn btn-warning btn-xs" onclick="return confirm('Deseja realmente excluir este item?')" >Remover</a>


                                                    </td>
                                                </tr>





                                                <?php } ?>













                                            </tbody>
                                        </table>


                                        <p>
                                            <label>Nome: </label> <?php echo "$nome_cliente" ?> <br>                                                
                                            <label>Endereço: </label> <?php echo "$endereco" ?><br>
                                            <label>Telefones:</label> <?php echo "$telefones" ?> <br>                                               
                                            <label>Data de retirada:</label> <?php echo setardata($inicio, "x");  ?> <br> 
                                            <label>Data de devolução:</label> <?php echo setardata($fim, "x");  ?> <br> 
                                            <label>Total de dias:</label> <?php echo $dias;  ?> <br> 
                                            <label>Diária: </label> R$ <?php echo number_format($diaria, 2, ',', '.'); ?><br><br>

                                            <a href="scriptz/confirmar_locacao.php?id=<?php echo "$id_locacao" ?>&tipo=item_na_lista" class="btn btn-primary btn-md" onclick="return confirm('Esta Locação está confirmada?')" >Confirmar</a>
                                            <a href="scriptz/deletar.php?id=<?php echo "$id_locacao" ?>&tipo=locacao" class="btn btn-danger btn-md right" onclick="return confirm('Deseja realmente excluir esta locação?')" >Cancelar locação</a>

                                        </p>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!---/Tabela --------------------------------------------------------->

                        <!--- Tabela de produtos --------------------------------------------------------->
                        <div class="col-md-6" id="tabela">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-file-text-o" aria-hidden="true"></i> Todos produtos
                                </div>
                                <div class="panel-body">



                                    <div class="table-responsive">
                                        <table class="table table-bordered" >
                                            <thead>
                                                <tr>
                                                    <th >Nome</th>
                                                    <th style="width: 90px">Preço</th>     
                                                    <th style="width: 70px">Qte</th>                                                    


                                                    <th  style="width: 60px"></th>

                                                </tr>

                                            </thead>
                                            <tbody>

                                                <?php

    $action = "inserir";                                            

                                               $query = mysqli_query($con,"SELECT * FROM produtos WHERE nome != '' ORDER by nome ASC ");    
                                               while($linha=mysqli_fetch_array($query)){

                                                   $id_produto = $linha['id'];
                                                   $nome = $linha['nome'];
                                                   $preco = $linha['preco'];
                                                   $grupo = $linha['grupo'];
                                                   $qte = $linha['qte'];
                                                   $cor = $linha['cor'];
                                                   $tamanhos = $linha['tamanhos'];
                                                   $categoria = $linha['categoria'];
                                                   $texto = $linha['texto'];

                                                   if($preco == ""){$preco = "0";}

                                                   if($grupo != "0"){

                                                       $query_grupo = mysqli_query($con,"SELECT * FROM grupos WHERE id = '$grupo' ");   
                                                       $linha_grupo=mysqli_fetch_array($query_grupo);

                                                       $preco = $linha_grupo['preco'];

                                                       $nome = "$nome - nº $tamanhos";
                                                   }





                                                ?>

                                                <tr >
                                                    <form method="post" action="scriptz/insere_na_lista.php" >
                                                        <td><?php echo "$nome" ?></td>
                                                        <td>R$ <?php echo number_format($preco, 2, ',', '.'); ?></td>

                                                        <td>    
                                                            <input type="number" class="form-control"  required name="qte"  min="1" max="<?php echo "$qte"?>" style="width: 60px" />


                                                        </td>


                                                        <td>
                                                            <button type="submit" href="?s=<?php echo "$id" ?>" class="btn btn-success btn-xs">inserir</button>

                                                            <input type="hidden" name="inicio" value="<?php echo "$inicio" ?>" >
                                                            <input type="hidden" name="fim" value="<?php echo "$fim" ?>" >
                                                            <input type="hidden" name="id_locacao" value="<?php echo "$id_locacao" ?>" >
                                                            <input type="hidden" name="id_produto" value="<?php echo "$id_produto" ?>" >
                                                            <input type="hidden" name="nome" value="<?php echo "$nome" ?>">

                                                        </td>
                                                    </form>
                                                </tr>






                                                <?php } ?>













                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!---/Tabela de produtos --------------------------------------------------------->


                    </div>





                </div>
                <!-- /. PAGE INNER  -->
            </div>
            <!-- /. PAGE INNER  -->
        </div>






    </body>
</html>
