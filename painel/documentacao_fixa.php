<?php include("includes/php/restrito.php") ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php include("includes/header.php") ?>






    </head>
    <body>
        <div id="wrapper">
            <?php include("includes/topo.php") ?>
            <!-- /. NAV TOP  -->
            <?php include("includes/navbar.php") ?>
            <div id="page-wrapper" >
                <div id="page-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Documentação fixa</h2>  

                            <?php
    $action = "inserir";

$titl = "Inserir";

if(isset($i)){

    $query = mysqli_query($con,"SELECT * FROM documentacao_fixa WHERE id = '$i' ");
    $action = "editar";


    $titl = "Editar ";
} else{


    $query = mysqli_query($con,"SELECT * FROM documentacao_fixa WHERE nome = '' ");
    $action = "inserir";

}





$linha=mysqli_fetch_array($query);

$id = $linha['id'];
$nome = $linha['nome'];
$texto = $linha['texto'];
$visibilidade = $linha['visibilidade'];

$setores = "Master,Médico associado,Médico não-associado,Coordenador,Supervisor,Colaborador";


if( strpos( $visibilidade, "Master" ) !== false ) {$marcado1 = "checked";}





                            ?>

                        </div>
                    </div>
                    <!-- /. ROW  -->

                    <div class="row">
                        <!--- Formulário --------------------------------------------------------->
                        <div class="col-md-12">
                            <!-- Form Elements -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Documentação fixa
                                </div>
                                <div class="panel-body">
                                    <div class="row">

                                        <div class="col-md-12">




                                            <h3>Inserir documento </h3>

                                          
                                            <form role="form" method="post" action="scriptz01/inserir_documentacao.php">

                                                <div class="form-group">
                                                    <label>Título</label>
                                                    <input class="form-control" required="" name="nome" value="<?php echo "$nome" ?>">
                                                </div>

                                                <div class="form-group">
                                                    <label>Visível para:</label>

                                                    <div class="cks total left">

                                                        <?php



    $i = "";
                                                           $marcado = "";
                                                           $string_setores = explode(",", $setores);
                                                           foreach($string_setores as $elemento)
                                                           {
                                                               ++$i;
                                                               echo "<label class='ck-button'>
                                                            <input name='visibilidade[]' $marcado$i type='checkbox' value='$elemento'><span>$elemento</span>
                                                        </label>
                                                        ";
                                                           }

                                                        ?>



                                                        <!-- Dados ------------------------------>











                                                    </div>



                                                    <!-- /Dados ----------------------------->

                                                </div>



                                                <div class="form-group">
                                                    <label class="total left">Imagem</label>
                                                    <a href="arquivos.php?d=1&vrf=<?php echo "$id" ?>&tipo=doc" title="" class="btn btn-success btn-md iframe">Documentos</a>
                                                </div>

                                                <div class="form-group">
                                                    <label>Texto</label>
                                                    <textarea class="form-control editorx02" name="texto" rows="6"><?php echo "$texto" ?></textarea>
                                                </div>



                                                <input type="hidden" name="action" value="<?php echo "$action" ?>">
                                                <input type="hidden" name="id" value="<?php echo "$id" ?>">
                                                <button type="submit" class="btn btn-default">inserir</button>


                                                <!-------------------------------------->


                                            </form>


                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Form Elements -->
                        </div>


                        <!---/Formulário --------------------------------------------------------->

                        <!--- Tabela --------------------------------------------------------->
                        <div class="col-md-12" id="tabela">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-file-text-o" aria-hidden="true"></i> Documentação fixa
                                </div>
                                <div class="panel-body">



                                    <div class="table-responsive">
                                        <table class="table table-bordered" >
                                            <thead>
                                                <tr>
                                                    <th >Documento</th>
                                                    <th style="width: 250px">Disponível para</th>

                                                    <th  style="width: 180px"></th>

                                                </tr>

                                            </thead>
                                            <tbody>



                                                <?php 

    $query = mysqli_query($con,"SELECT * FROM documentacao_fixa WHERE nome != '' ");

                                                while($linha=mysqli_fetch_array($query)){

                                                    $id = $linha['id'];
                                                    $nome = $linha['nome'];
                                                    $visibilidade = $linha['visibilidade'];
                                                    

                                                ?>
                                                <tr class="">
                                                    <td><?php echo "$nome"?></td>
                                                    <td><?php echo rtrim(str_replace(",", ", ", "$visibilidade"),", "); ?></td>


                                                    <td>
                                                        <a href="?i=<?php echo "$id" ?>" class="btn btn-success btn-xs">Editar</a>
                                                        <a href="arquivos.php?vrf=<?php echo "$id" ?>&tipo=doc" class="btn btn-primary btn-xs iframe">Imagens</a>
                                                        <a href="scriptz/deletar.php?id=<?php echo "$id" ?>&tipo=documento" class="btn btn-danger btn-xs" onclick="return confirm('Deseja realmente excluir este item?')" >Excluir</a>


                                                    </td>
                                                </tr>

                                                <?php }?>


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!---/Tabela --------------------------------------------------------->

                    </div>





                </div>
                <!-- /. PAGE INNER  -->
            </div>
            <!-- /. PAGE INNER  -->
        </div>






    </body>
</html>
