<?php

include("../includes/php/restrito.php");
include("../includes/php/mysql_con.php");
include("../includes/php/anti_sql_injection.php");


if (isset($_POST['submit']))
{

    function subir($arquivo, $input_nome, $documento, $vrf,$con ){

        if($arquivo != ""){

            $hjSQL = date("Y-m-d");

            //$filename = $_FILES["arquivo"]["name"];
            $filename = "$arquivo";
            $file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
            $file_ext = substr($filename, strripos($filename, '.')); // get file name
            //$filesize = $_FILES["arquivo"]["size"];
            $filesize = $_FILES["$input_nome"]["size"];
            $allowed_file_types = array('.doc','.docx','.jpg','.pdf','.jpeg', 'png');	

            if (in_array($file_ext,$allowed_file_types) && ($filesize < 2000000))
            {	
                // Rename file
                $newfilename = md5(uniqid(rand(), true)).$file_ext;

                if (file_exists("upload/" . $newfilename))
                {
                    // file already exists error
                    //echo "You have already uploaded this file.";
                }
                else
                {		
                    move_uploaded_file($_FILES["$input_nome"]["tmp_name"], "../uploads/" . $newfilename);
                    //echo "File uploaded successfully.";	

                    $sql = "INSERT INTO documentos (arquivo, documento, vrf, status, data_envio) VALUES ('$newfilename', '$documento',  '$vrf', 'pendente', '$hjSQL')";

                    //echo "$sql";
                    $resultado = mysqli_query($con,$sql)
                        or die (mysqli_error());

                    echo "<script language='Javascript' type='text/javascript'> alert('$documento enviado com sucesso, aguarde a conferência');history.go(-1);</script>";
                }
            }
            elseif (empty($file_basename))
            {	
                echo '<script language="Javascript" type="text/javascript"> alert("Selecione os arquivos para upload");history.go(-1);</script>';

            } 
            elseif ($filesize > 2000000)
            {	
                echo '<script language="Javascript" type="text/javascript"> alert("Este arquivo é muito grande para upload.");history.go(-1);</script>';


            }
            else
            {
                // file type error

                $somente_arquivos = "Somente estes tipos de arquivo são permitidos: " . implode(', ',$allowed_file_types);

                echo "<script language='Javascript' type='text/javascript'> alert('$somente_arquivos');history.go(-1);</script>";

                unlink($_FILES["arquivo"]["tmp_name"]);
            }
        }
    }

    $arquivo01 = $_FILES["arquivo01"]["name"];
    $arquivo02 = $_FILES["arquivo02"]["name"];
    $arquivo03 = $_FILES["arquivo03"]["name"];
    $arquivo04 = $_FILES["arquivo04"]["name"];
    $arquivo05 = $_FILES["arquivo05"]["name"];
    $arquivo06 = $_FILES["arquivo06"]["name"];

    $id_usuário = $_POST['vrf'];



    subir($arquivo01, 'arquivo01', 'Procuração assinada', $id_usuário,$con);
    subir($arquivo02, 'arquivo02', 'Declaração de hiposuficiência assinada ', $id_usuário,$con);
    subir($arquivo03, 'arquivo03', 'Contrato assinado ', $id_usuário,$con);
    subir($arquivo04, 'arquivo04', 'Cópia da identidade ', $id_usuário,$con);
    subir($arquivo05, 'arquivo05', 'Cópia do CPF ', $id_usuário,$con );
    subir($arquivo06, 'arquivo06', 'Comprovante de endereço ', $id_usuário,$con);


}

?>