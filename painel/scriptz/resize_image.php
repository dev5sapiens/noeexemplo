<?php


function resize_image($fonte,$nome,$largura,$pasta_destino){


    $extension = pathinfo($fonte, PATHINFO_EXTENSION);


    if($extension == "png"){
        $img = imagecreatefrompng("$fonte");
    }else{
        $img = imagecreatefromjpeg("$fonte");   
    }

  
    $data = getimagesize($fonte);
    $x = $data[0];
    $y = $data[1];

    $altura = ($largura * $y) /$x;

    $nova = imagecreatetruecolor($largura, $altura);
    imagealphablending($nova, false);
    imagesavealpha($nova,true);
    $transparent = imagecolorallocatealpha($nova, 255, 255, 255, 127);
    imagefilledrectangle($nova, 0, 0, $largura, $altura, $transparent);
    imagecopyresampled($nova, $img, 0, 0, 0, 0, $largura, $altura, $x, $y);

    imagepng($nova, "$pasta_destino/$nome");

    imagedestroy($nova);
    imagedestroy($img);

}

// in your php pages create images with a code like this:
// create_square_image("sample.jpg","sample_thumb.jpg",200);
// first parameter is the name of the image file to resize
// second parameter is the path where you would like to save the new square thumb, e.g. "sample_thumb.jpg" or just "NULL" if you do not want to save new image. If NULL then this file should be used as the "src" of the image. Folder whre you save image has to be writable, "777" permission code on most servers.
// 200 is the size of the new square thumb

//create_square_image("sample.jpg","sample_thumb.jpg",200);

?>


