<?php

// Variáveis nulas
$erro = 0;
$msg_error = "";
$frase = "Item inserido com sucesso";
$hjSQL = date("Y-m-d");


// Inclui os arquivos necessários pra abrir a página sem zica

if(!empty($_POST)){
    include("../includes/animations/loading.php");
    include("../includes/php/restrito.php");
    include("../includes/php/mysql_con.php");
    include("../includes/funcoes/datas.php");
    include("../includes/funcoes/enviar_email.php");

    $tabela = "atividades";

    // Gera para cada input uma variável correspondente     (foreach ($_POST as $campo => $valor) { $$campo = $valor;})
    foreach ($_POST as $campo => $valor) { $$campo = $valor;}

    $fim = getPraticaDataFim($id_pratica, $con);

    $data_envio = setardata($fim, "x");

    $pagina = "associar";



// Inserir em grupo =======================================================
    if($tipo == "grupo"){
        if(empty($responsavel) ){
            ++$erro;
            $msg_erro = "Escolha os responsáveis pela documentação";
        }

        if($erro == 0){
            foreach($_POST['responsavel'] as $id_grupo) {
                // eg. "I have a grapefruit!"
                echo "$id_grupo";
                // -- insert into database call might go here
            }

            // eg. "apple, grapefruit"
            $sql_users = implode(')%\' OR grupos LIKE \'%(', $_POST['responsavel']);
            $sql_users = "SELECT * FROM usuarioz WHERE grupos LIKE '%($sql_users)%'";

            // -- insert into database call (for query_users) might go here.


            //$query_user = mysqli_query($con,"SELECT * FROM usuarioz WHERE setor01 = '$setor'  OR setor02 = '$setor' OR setor03 = '$setor'  ORDER BY nome ");   
            $query_user = mysqli_query($con,"$sql_users");
            $qte_user = mysqli_num_rows($query_user);

            if($qte_user == 0){
                ++$erro;
                $msg_erro = "Não existem usuários nestes grupos";
            }

            if($qte_user > 0){
                while($linha_user=mysqli_fetch_array($query_user)){

                    $id_user = $linha_user['id'];
                    $nome_user = $linha_user['nome'];

                    $query_id = mysqli_query($con,"SELECT * FROM atividades WHERE responsavel ='$id_user' AND id_pratica = '$id_pratica' ");
                    $qte_id_pratica = mysqli_num_rows($query_id);

                    if($qte_id_pratica == 0){

                        $sql = "INSERT INTO $tabela (inicio,fim,responsavel,id_pratica) VALUES 
                            ('$hjSQL','$fim','$id_user','$id_pratica')";
                        
                        echo "<div class='loader'></div>";

                        $resultado = mysqli_query($con,$sql)
                            or die (mysqli_error($con));




                        // Enviar mensagem =======================================================

                        $query_responsavel = mysqli_query($con,"SELECT * FROM usuarioz WHERE id = '$id_user' ");
                        $linha_responsavel=mysqli_fetch_array($query_responsavel);

                        $nome_user = $linha_responsavel['nome'];
                        $email_user = $linha_responsavel['email'];

                        $email_user = "$email_user";

                        //echo "$email_user<br>";

                        $assunto = "Novo atividade ";
                        $mensagem = "
            Olá $nome_user 
            Nova Atividade inserida
            $nome_user

            Uma nova atividade foi inserida no painel de controle a ser concluída até $data_envio.

            <a href='http://nucleomg.com.br/painel/'>Painel de controle NOE</a>
            ";

                        enviar_email("$email_user", "$assunto",  "$mensagem");

                        // Enviar mensagem =======================================================
                    }
                }
            }
        }
    }
    // /Inserir em grupo =======================================================
   
    
    // Inserir individualmente  =======================================================
    if($tipo == "individual"){
            //$query_user = mysqli_query($con,"SELECT * FROM usuarioz WHERE setor01 = '$setor'  OR setor02 = '$setor' OR setor03 = '$setor'  ORDER BY nome ");   
            $query_user = mysqli_query($con,"SELECT * FROM usuarioz WHERE nome = '$nome_usuario'");
            $qte_user = mysqli_num_rows($query_user);

            if($qte_user == 0){
                ++$erro;
                $msg_erro = "Não existe usuário com este nome";
            }

            if($qte_user > 0){
               $linha_user=mysqli_fetch_array($query_user);

                    $id_user = $linha_user['id'];
                    $nome_user = $linha_user['nome'];

                    $query_id = mysqli_query($con,"SELECT * FROM atividades WHERE responsavel ='$id_user' AND id_pratica = '$id_pratica' ");
                    $qte_id_pratica = mysqli_num_rows($query_id);

                    if($qte_id_pratica == 0){

                        $sql = "INSERT INTO $tabela        

            (inicio,fim,responsavel,id_pratica)         
            VALUES         
            ('$hjSQL','$fim','$id_user','$id_pratica')        
            ";
                //echo '<pre>'; print_r($sql);die;
                echo "<div class='loader'></div>";

                        $resultado = mysqli_query($con,$sql)
                            or die (mysqli_error($con));




                        // Enviar mensagem =======================================================

                        $query_responsavel = mysqli_query($con,"SELECT * FROM usuarioz WHERE id = '$id_user' ");
                        $linha_responsavel=mysqli_fetch_array($query_responsavel);

                        $nome_user = $linha_responsavel['nome'];
                        $email_user = $linha_responsavel['email'];

                        $email_user = "$email_user";

                        //echo "$email_user<br>"; die;



                        $assunto = "Novo atividade ";
                        $mensagem = "
            Olá $nome_user 
            Nova Atividade inserida
            $nome_user

            Uma nova atividade foi inserida no painel de controle a ser concluída até $data_envio.

            <a href='http://nucleomg.com.br/painel/'>Painel de controle NOE</a>
            ";

                        enviar_email("$email_user", "$assunto",  "$mensagem");

                        // Enviar mensagem =======================================================
                    }
                }
            }
  
    // /Inserir individualmente  =======================================================
}


function getPraticaDataFim($pratica_id, $con) {
    
    //Seleciona a data final da atividade
    $query_fim_pratica = mysqli_query($con,"SELECT fim FROM praticas WHERE id = '$pratica_id'");
    $fim_pratica_array = mysqli_fetch_array($query_fim_pratica);
    $fim_pratica = $fim_pratica_array['fim'];
    return $fim_pratica;
}



if($erro > 0){echo '<script language="Javascript" type="text/javascript"> alert("'.$msg_erro.'");history.go(-1);</script>' ; }

if($erro == 0){echo '<script language="Javascript" type="text/javascript"> alert("'.$frase.'");window.location = "../'.$pagina.'.php?i='.$id_pratica.'";</script>' ;}









?>