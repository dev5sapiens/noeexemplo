<?php include("includes/php/restrito.php") ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php include("includes/header.php") ?>
    </head>
    <body>
        <div id="wrapper">
            <?php include("includes/topo.php") ?>
            <!-- /. NAV TOP  -->
            <?php include("includes/navbar.php") ?>
            <div id="page-wrapper" >
                <div id="page-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Início</h2>   

                        </div>
                    </div>
                    <!-- /. ROW  -->

                    <div class="row hide">
                        <div class="col-md-4 col-sm-6 col-xs-6">           
                            <div class="panel panel-back noti-box">

                                <span class="icon-box bg-color-red set-icon">
                                    <i class="fa fa-envelope-o"></i>
                                </span>
                                <div class="text-box">
                                    <p class="main-text">3 avisos</p>
                                    <p class="text-muted"> Ver Documentos</p>
                                </div>

                            </div>

                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-6">           
                            <div class="panel panel-back noti-box">
                                <span class="icon-box bg-color-green set-icon">
                                    <i class="fa fa-bars"></i>
                                </span>
                                <div class="text-box">
                                    <p class="main-text">3 avisos </p>
                                    <p class="text-muted"> Ver Chamados</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-6">           
                            <div class="panel panel-back noti-box">
                                <span class="icon-box bg-color-blue set-icon">
                                    <i class="fa fa-bell-o"></i>
                                </span>
                                <div class="text-box">
                                    <p class="main-text">3 avisos</p>
                                    <p class="text-muted"> Ver Notificações</p>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-12">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-file-text-o" aria-hidden="true"></i></i> Chamados
                            </div>
                            <div class="panel-body">

                                <h3>Histórico de chamados</h3>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>

                                                <th>Tabela de chamados</th>

                                                <th  style="width: 100px"></th>

                                            </tr>

                                        </thead>
                                        <tbody>

                                            <?php 
    // Item multiplo +  Galeria + Last
    $last = 0;
$query = mysqli_query($con,"SELECT * FROM chamados WHERE id_user='$id_user' ORDER BY  id DESC");
while ($linha=mysqli_fetch_array($query)) { 

    $id =$linha['id'];
    $assunto =$linha['assunto'];
    $texto02 =$linha['texto02'];

    $data_envio = implode('/', array_reverse(explode('-', $linha['data_envio'])));

    $resposta = "warning";
    $status = "Aguardando";

    if($texto02 != ""){
        $resposta = "success";
        $status = "Respondido"; 
    }



                                            ?>

                                            <tr>
                                                <td><?php echo"$data_envio - $assunto"?></td>
                                                <td>
                                                    <span class="label label-<?php echo "$resposta"?>"><?php echo "$status"?></span>

                                                </td>
                                            </tr>

                                            <?php  

}
                                            ?>














                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>




                <!--- If se existir processo ----------------------------->                    
                <?php // if(){} ?>
                <?php  ?>
                <!--- If se existir processo ----------------------------->

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Dados do processo
                            </div>

                            <div class="panel-body">

                                <p>
                                    <strong>Número do processo: </strong> <br>
                                    <strong>Cliente: </strong> <br>
                                    <strong>Comarca: </strong> <br>
                                    <strong>Parte adversa: </strong> <br>
                                    <strong>Próxima audiência: </strong> <br>
                                    <strong>Situação: </strong> <br>

                                </p>


                            </div>


                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <!-- Form Elements -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i></i> Gerar documentação
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Instruções</h3>
                                    <p>Primeiramente gere os documentos clicando nos links abaixo, em seguida imprima-os, assine e nos encaminhe através do painel abaixo deste.</p>
                                    <p>É importante que todas informações estejam todas corretas, caso alguma este errada ou deseja atualizar <a href="perfil.php">perfil do usuário</a></p>   
                                    <style>
                                        .btn_gnr{font-size: 13px}
                                    </style>
                                    <a href="pdf_procuracao.php?user=<?php echo "$id_user" ?>" target="_blank" class="btn btn-primary btn-lg btn_gnr"><strong>Gerar procuração</strong></a>
                                    <a href="pdf_contrato.php?user=<?php echo "$id_user" ?>" target="_blank" class="btn btn-primary btn-lg btn_gnr"><strong>Gerar Contrato</strong></a>
                                    <a href="pdf_declaracao.php?user=<?php echo "$id_user" ?>" target="_blank" class="btn btn-primary btn-lg  btn_gnr"><strong>Gerar Decaração de hiposuficiência</strong></a>

                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i></i> Documentação pendente
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h3>Anexar documentos pendentes</h3>
                                <p>
                                    Caso não tenha todos  documentos envie os que você tiver e mais tarde retorne.
                                </p>


                                <script>
                                    var SITE = SITE || {};

                                    SITE.fileInputs = function() {
                                        var $this = $(this),
                                            $val = $this.val(),
                                            valArray = $val.split('\\'),
                                            newVal = valArray[valArray.length-1],
                                            $button = $this.siblings('.button'),
                                            $fakeFile = $this.siblings('.file-holder');
                                        if(newVal !== '') {
                                            $button.text('Arquivo selecionado');
                                            $button.css("background", "#2c86d9" );


                                            if($fakeFile.length === 0) {
                                                $button.after('<span class="file-holder">' + newVal + '</span>');
                                            } else {
                                                $fakeFile.text(newVal);
                                            }
                                        }
                                    };

                                    $(document).ready(function() {
                                        $('.file-wrapper input[type=file]').bind('change focus click', SITE.fileInputs);
                                    });
                                </script>

                                <style>

                                    .formbox {
                                        width: 100%;
                                    }

                                    .file-wrapper {cursor: pointer;display: inline-block;overflow: hidden;position: relative;}
                                    .file-wrapper input {cursor: pointer;font-size: 100px;height: 100%;opacity: 0.01;position: absolute;right: 0;top: 0; background: #eee}
                                    .file-wrapper .button {background: #eee;border-radius: 5px;color: black;cursor: pointer;display: inline-block;font-weight: bold;margin-right: 5px;padding: 7px 20px;text-transform: uppercase;}
                                    .file-holder{         padding: 12px 15px;}                     
                                </style>

                                <form role="form" method="post" action="scriptz/upload_docs.php" enctype="multipart/form-data"  >


                                    <!-------------------------------------->



                                    <div class="form-group">
                                        <span class="file-wrapper">
                                            <input type="file" name="arquivo01" id="photo" />
                                            <span class="button"><i class="fa fa-paperclip" aria-hidden="true"></i> Procuração assinada</span>
                                        </span>
                                    </div>

                                    <div class="form-group">
                                        <span class="file-wrapper">
                                            <input type="file" name="arquivo02" id="photo" />
                                            <span class="button"><i class="fa fa-paperclip" aria-hidden="true"></i> Declaração de hiposuficiência assinada</span>
                                        </span>
                                    </div>

                                    <div class="form-group">
                                        <span class="file-wrapper">
                                            <input type="file" name="arquivo03" id="photo" />
                                            <span class="button"><i class="fa fa-paperclip" aria-hidden="true"></i> Contrato assinado</span>
                                        </span>
                                    </div>                             

                                    <div class="form-group">
                                        <span class="file-wrapper">
                                            <input type="file" name="arquivo04" id="photo" />
                                            <span class="button"><i class="fa fa-paperclip" aria-hidden="true"></i> Cópia da identidade</span>
                                        </span>
                                    </div>


                                    <div class="form-group">
                                        <span class="file-wrapper">
                                            <input type="file" name="arquivo05" id="photo" />
                                            <span class="button"><i class="fa fa-paperclip" aria-hidden="true"></i> Cópia do CPF (Caso não conste na identidade)</span>
                                        </span>
                                    </div>


                                    <div class="form-group">
                                        <span class="file-wrapper">
                                            <input type="file" name="arquivo06" id="photo" />
                                            <span class="button"><i class="fa fa-paperclip" aria-hidden="true"></i> Comprovante de endereço</span>
                                        </span>
                                    </div>

                                    <input type="hidden" name="vrf" value="<?php echo "$id_user" ?>" >







                                    <!-------------------------------------->

                                    <button type="submit" name="submit" class="btn btn-success">Enviar documentos</button>


                                </form>



                            </div>


                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>

            <div class="col-md-6 col-sm-12 col-xs-12">
<?php include("includes/documentos_enviados.php") ?>
             
        </div>
        </div>





    </div>
<!-- /. PAGE INNER  -->
</div>
<!-- /. PAGE INNER  -->
</div>



</body>
</html>
