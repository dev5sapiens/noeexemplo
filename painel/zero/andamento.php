<?php include("includes/php/restrito.php") ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php include("includes/header.php") ?>






    </head>
    <body>
        <div id="wrapper">
            <?php include("includes/topo.php") ?>
            <!-- /. NAV TOP  -->
            <?php include("includes/navbar.php") ?>
            <div id="page-wrapper" >
                <div id="page-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Nome do serviço</h2>   

                        </div>
                    </div>
                    <!-- /. ROW  -->

                    <div class="row ">


                    <div class="col-md-12" id="tabela">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i></i>Nome do serviço 
                        </div>
                        <div class="panel-body">



                            <div class="table-responsive">
                                <table class="table table-bordered" >
                                    <thead>
                                        <tr>
                                            <th style="width: 250px">Serviço</th>
                                            <th>Cliente</th>                                                    
                                            <th style="width: 90px">Solicitado </th>

                                            <th  style="width: 200px"></th>

                                        </tr>

                                    </thead>
                                    <tbody>

                                        <?php


                                        $query = mysqli_query($con,"SELECT * FROM solicitacoes WHERE situacao = 'andamento' ORDER by id DESC ");    
                                        while($linha=mysqli_fetch_array($query)){

                                            $id = $linha['id'];
                                            $nome = $linha['nome'];
                                            $cliente = "Nome do cliente";
                                            $data_contratacao = $linha['data_contratacao'];


                                            $data_contratacao = implode('/', array_reverse(explode('-', $linha['data_contratacao'])));


                                        ?>

                                        <tr class="">
                                            <td><?php echo "$nome" ?></td>
                                            <td><?php echo "$cliente" ?></td>
                                            <td><?php echo "$data_contratacao" ?></td>


                                            <td>                                                    
                                                <a href="lista_historico_servico.php?u=<?php echo "$id" ?>&act=inserir" class="btn btn-warning btn-xs">Histórico</a>
                                                <a href="solicitacao_dados.php?id=<?php echo "$id" ?>" class="btn btn-primary btn-xs iframe">Visualizar</a>
                                                    <a href="scriptz/concluir.php?id=<?php echo "$id" ?>" class="btn btn-danger btn-xs" onclick="confirm('Este serviço deve  ser concluido?')">Concluir</a>


                                            </td>
                                        </tr>





                                        <?php } ?>



                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-12" id="tabela">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i></i>Serviços concluídos
                    </div>
                    <div class="panel-body">



                        <div class="table-responsive">
                            <table class="table table-bordered" >
                                <thead>
                                    <tr>
                                        <th style="width: 250px">Serviço</th>
                                        <th>Cliente</th>                                                    
                                        <th style="width: 90px">Solicitado </th>
                                        <th style="width: 90px">Finalizado </th>

                                        <th  style="width: 140px"></th>

                                    </tr>

                                </thead>
                                <tbody>

                                    <?php


                                    $query = mysqli_query($con,"SELECT * FROM solicitacoes WHERE situacao = 'terminou' ORDER by id DESC ");    
                                    while($linha=mysqli_fetch_array($query)){

                                        $id = $linha['id'];
                                        $nome = $linha['nome'];
                                        $cliente = "Nome do cliente";

                                        $data_contratacao = implode('/', array_reverse(explode('-', $linha['data_contratacao'])));
                                       
                                        
                                        $data_finalizacao= implode('/', array_reverse(explode('-', $linha['data_finalizacao'])));
                                        

                                    ?>

                                    <tr class="">
                                        <td><?php echo "$nome" ?></td>
                                        <td><?php echo "$cliente" ?></td>
                                        <td><?php echo "$data_contratacao" ?></td>
                                        <td><?php echo "$data_finalizacao" ?></td>


                                        <td>                                                    

                                            <a href="lista_historico_servico.php?u=<?php echo "$id" ?>&act=inserir" class="btn btn-warning btn-xs">Histórico</a>
                                               <a href="solicitacao_dados.php?id=<?php echo "$id" ?>" class="btn btn-primary btn-xs iframe">Visualizar</a>
                                                    

                                        </td>
                                    </tr>





                                    <?php } ?>



                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>




        </div>





        </div>
    <!-- /. PAGE INNER  -->
    </div>
<!-- /. PAGE INNER  -->
</div>






</body>
</html>
