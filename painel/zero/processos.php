<?php include("includes/php/restrito.php") ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php include("includes/header.php") ?>






    </head>
    <body>
        <div id="wrapper">
            <?php include("includes/topo.php") ?>
            <!-- /. NAV TOP  -->
            <?php include("includes/navbar.php") ?>
            <div id="page-wrapper" >
                <div id="page-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Processos abertos</h2>   

                        </div>
                    </div>
                    <!-- /. ROW  -->

                    <div class="row">
                        <div class="col-md-12">
                            <!-- Form Elements -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    z04
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">


                                            <?php
    $action = "inserir";

$titl = "Inserir";
if(isset($tp)){

    $titl = "Marcar audiencia para  ";

    $query = mysqli_query($con,"SELECT * FROM processos WHERE id = '$u' ");
    $action = "editar";

    echo "<style> 
    .form-group{display: none}
    .block{display: block}

    </style>
    ";

    $instrucao = "block";

}

elseif(isset($u)){

    $query = mysqli_query($con,"SELECT * FROM processos WHERE id = '$u' ");
    $action = "editar";
    
    $instrucao = "hide";

    $titl = "Editar dados do ";
} else{


    $query = mysqli_query($con,"SELECT * FROM processos WHERE id_usuario = '' ");   
    $instrucao = "hide";

}
$linha=mysqli_fetch_array($query);

$id_processo = $linha['id'];
$id_usuario = $linha['id_usuario'];
$parte_adversa = $linha['parte_adversa'];
$comarca = $linha['comarca'];
$situacao = $linha['situacao'];
$numero = $linha['numero'];
$texto = $linha['texto'];
$audiencia = $linha['audiencia'];
$valorfinal= $linha['valorfinal'];
$resultado= $linha['resultado'];

$data = implode('/', array_reverse(explode('-', $linha['audiencia'])));
if($data == "00/00/0000") {$data = date("d/m/Y");}


                                            ?>
                                            <h3 ><?php echo "$titl processo: $numero"?> </h3>

                                            <form role="form" method="post" action="includes/inserir_processo.php">

                                                <div class="form-group">
                                                    <label>Nome do usuário</label>
                                                    <select name="id_usuario" class="form-control">

                                                        <?php

    $query_user = mysqli_query($con,"SELECT * FROM usuarioz WHERE id = '$id_usuario' ");
                                                while($linha_user=mysqli_fetch_array($query_user)){

                                                    $id = $linha_user['id'];
                                                    $nome = $linha_user['nome_completo'];

                                                    echo "<option value='$id'>$nome</option>";

                                                }               
                                                        ?>

                                                        <option> </option>
                                                        <?php

                                                        $query_user = mysqli_query($con,"SELECT * FROM usuarioz WHERE tipo = 'user' AND status = 'ok' ORDER BY  nome DESC");
                                                        while($linha_user=mysqli_fetch_array($query_user)){

                                                            $id = $linha_user['id'];
                                                            $nome = $linha_user['nome_completo'];

                                                            echo "<option value='$id'>$nome</option>";

                                                        }               
                                                        ?>



                                                    </select>
                                                </div>



                                                <div class="form-group">
                                                    <label>Número do processo</label>
                                                    <input class="form-control" name="numero" value="<?php echo "$numero" ?>" style="width: 200px" />
                                                </div>

                                                <div class="form-group">
                                                    <label>Comarca</label>
                                                    <input class="form-control" name="comarca" value="<?php echo "$comarca" ?>"  style="width: 200px" />
                                                </div>

                                                <div class="form-group">
                                                    <label>Parte Adversa</label>
                                                    <input class="form-control" name="parte_adversa" value="<?php echo "$parte_adversa" ?>"  style="width: 400px" />
                                                </div>

                                                <div class="form-group <?php echo "$instrucao"?>" >
                                                    <label>Audiência</label>
                                                    <input type='text' class="form-control datepicker-here" name="audiencia" value="" data-language='pt-BR'  data-position="bottom left"  style="width: 120px" />
                                                </div>




                                                <div class="form-group">
                                                    <label>Detalhes adicionais</label>
                                                    <textarea class="form-control" name="texto" rows="6"><?php echo "$texto" ?></textarea>
                                                </div>


                                                <input type="hidden" name="data_registro" value="<?php echo "$hjSQL" ?>" >
                                                <input type="hidden" name="valorfinal" value="" >
                                                <input type="hidden" name="resultado" value="" >
                                                <input type="hidden" name="pagina" value="processos" >
                                                <input type="hidden" name="tipo" value="aberto" >
                                                <input type="hidden" name="id" value="<?php echo "$id_processo" ?>" >
                                                <button type="submit" class="btn btn-default"><?php echo "$action"?></button>


                                                <!-------------------------------------->


                                            </form>


                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Form Elements -->
                        </div>


                        <div class="col-md-12" id="tabela">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-file-text-o" aria-hidden="true"></i></i> Processos sem audiencia
                                </div>
                                <div class="panel-body">



                                    <div class="table-responsive">
                                        <table class="table table-bordered" >
                                            <thead>
                                                <tr>
                                                    <th  style="width: 120px">N° Processo </th>
                                                    <th>Cliente</th>                                                    
                                                    <th  style="width: 90px">Comarca </th>
                                                    <th  style="width: 120px">Parte Adversa</th>

                                                    <th  style="width: 270px">Visualizar</th>

                                                </tr>

                                            </thead>
                                            <tbody>

                                                <?php

    $action = "inserir";                                            

                                                           $query = mysqli_query($con,"SELECT * FROM processos WHERE id_usuario != '' AND tipo = 'aberto' AND audiencia ='0000-00-00' ");    
                                                           while($linha=mysqli_fetch_array($query)){

                                                               $id = $linha['id'];
                                                               $id_usuario = $linha['id_usuario'];
                                                               $parte_adversa = $linha['parte_adversa'];
                                                               $comarca = $linha['comarca'];
                                                               $situacao = $linha['situacao'];
                                                               $numero = $linha['numero'];
                                                               $texto = $linha['texto'];                                                

                                                               $data = implode('/', array_reverse(explode('-', $linha['audiencia'])));
                                                               if($data == "00/00/0000") {$data = date("d/m/Y");}

                                                               $query_user = mysqli_query($con,"SELECT * FROM usuarioz WHERE id = '$id_usuario' ");
                                                               $linha_user=mysqli_fetch_array($query_user);


                                                               $nome = $linha_user['nome_completo'];





                                                ?>

                                                <tr class="">
                                                    <td><?php echo "$numero" ?></td>
                                                    <td><?php echo "$nome" ?></td>
                                                    <td><?php echo "$comarca" ?></td>
                                                    <td><?php echo "$parte_adversa" ?></td>


                                                    <td>
                                                        <a href="lista_historico.php?u=<?php echo "$id" ?>&act=inserir" class="btn btn-warning btn-xs">Histórico</a>
                                                        <a href="?u=<?php echo "$id" ?>" class="btn btn-success btn-xs">Editar</a>
                                                        <a href="?u=<?php echo "$id" ?>&tp=mrc" class="btn btn-primary btn-xs">Marcar audiencia</a>
                                                        <a href="processos_concluidos.php?u=<?php echo "$id" ?>&t=exclusao" class="btn btn-danger btn-xs" onclick="confirm('Este processo foi totalmente concluído?')">Excluir</a>


                                                    </td>
                                                </tr>





                                                <?php } ?>













                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12" id="tabela">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-file-text-o" aria-hidden="true"></i> Processos em andamento
                                </div>
                                <div class="panel-body">



                                    <div class="table-responsive">
                                        <table class="table table-bordered" >
                                            <thead>
                                                <tr>
                                                    <th  style="width: 120px">N° Processo </th>
                                                    <th>Cliente</th>                                                    
                                                    <th  style="width: 90px">Comarca </th>
                                                    <th  style="width: 120px">Parte Adversa</th>
                                                    <th  style="width: 110px">Audiência</th>
                                                    <th  style="width: 180px">Visualizar</th>

                                                </tr>

                                            </thead>
                                            <tbody>

                                                <?php

                                                $action = "inserir";                                            

                                                $query = mysqli_query($con,"SELECT * FROM processos WHERE id_usuario != '' AND tipo = 'aberto' AND audiencia !='0000-00-00' ORDER BY audiencia ASC ");    
                                                while($linha=mysqli_fetch_array($query)){

                                                    $id = $linha['id'];
                                                    $id_usuario = $linha['id_usuario'];
                                                    $parte_adversa = $linha['parte_adversa'];
                                                    $comarca = $linha['comarca'];
                                                    $situacao = $linha['situacao'];
                                                    $numero = $linha['numero'];
                                                    $texto = $linha['texto'];                                                
                                                    $dataSQL = $linha['audiencia'];                                                

                                                    $data = implode('/', array_reverse(explode('-', $linha['audiencia'])));
                                                    if($data == "00/00/0000") {$data = date("d/m/Y");}

                                                    $query_user = mysqli_query($con,"SELECT * FROM usuarioz WHERE id = '$id_usuario' ");
                                                    $linha_user=mysqli_fetch_array($query_user);


                                                    $nome = $linha_user['nome_completo'];





                                                ?>

                                                <tr class="">
                                                    <td><?php echo "$numero" ?></td>
                                                    <td><?php echo "$nome" ?></td>
                                                    <td><?php echo "$comarca" ?></td>
                                                    <td><?php echo "$parte_adversa" ?></td>
                                                    
                                                    <?php if ($dataSQL < $hjSQL){ ?>
                                                    <td><?php echo "$data" ?> <a href="includes/zerar_data.php?z=<?php echo "$id" ?>" class="btn btn-danger btn-xs" style="line-height: 1;border-radius: 16px;height: 18px;margin-left: 5px;" onclick="confirm('Deseja remover a data de audiência?')">x</a></td>
                                                    <?php }else{ ?>
                                                    <td><?php echo "$data" ?></td>
                                                    <?php } ?>
                                                  <td>
                                                        <a href="lista_historico.php?u=<?php echo "$id" ?>&act=inserir" class="btn btn-warning btn-xs">Histórico</a>
                                                        <a href="?u=<?php echo "$id" ?>" class="btn btn-success btn-xs">Editar</a>
                                                        <a href="processos_concluidos.php?u=<?php echo "$id" ?>&t=conclusao" class="btn btn-primary btn-xs " onclick="confirm('Este processo foi totalmente concluído?')">Concluir</a>
                                                        

                                                    </td>
                                                </tr>





                                                <?php } ?>













                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>





                </div>
                <!-- /. PAGE INNER  -->
            </div>
            <!-- /. PAGE INNER  -->
        </div>






    </body>
</html>
