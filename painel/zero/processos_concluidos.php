<?php include("includes/php/restrito.php") ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php include("includes/header.php") ?>



        <?php

    $titulo = "";

if($t == "conclusao"){
    $titulo = "concluídos";
}

if($t == "exclusao"){
    $titulo = "excluídos";
}
        ?>



    </head>
    <body>
        <div id="wrapper">
            <?php include("includes/topo.php") ?>
            <!-- /. NAV TOP  -->
            <?php include("includes/navbar.php") ?>
            <div id="page-wrapper" >
                <div id="page-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Processos <?php echo "$titulo"?></h2>   

                        </div>
                    </div>
                    <!-- /. ROW  -->


                    <div class="row">

                        <?php
    if(isset($u)){

                        ?>


                        <div class="col-md-12">
                            <!-- Form Elements -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    z04
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h3>Editar conclusão de processo: </h3>

                                            <?php
        $action = "inserir";

        if(isset($u)){

            $query = mysqli_query($con,"SELECT * FROM processos WHERE id = '$u' ");

            $linha=mysqli_fetch_array($query);

            $id = $linha['id'];
            $id_usuario = $linha['id_usuario'];
            $parte_adversa = $linha['parte_adversa'];
            $comarca = $linha['comarca'];
            $situacao = $linha['situacao'];
            $numero = $linha['numero'];
            $texto = $linha['texto'];
            $audiencia = $linha['audiencia'];
            $valorfinal= $linha['valorfinal'];
            $resultado= $linha['resultado'];

            $data = implode('/', array_reverse(explode('-', $linha['audiencia'])));
            if($data == "00/00/0000") {$data = date("d/m/Y");}


                                            ?>


                                            <form role="form" method="post" action="includes/inserir_processo.php">

                                                <div class="form-group">
                                                    <label>Nome do usuário</label>
                                                    <select name="id_usuario" class="form-control">

                                                        <?php

            $query_user = mysqli_query($con,"SELECT * FROM usuarioz WHERE id = '$id_usuario' ");
            while($linha_user=mysqli_fetch_array($query_user)){

                $id = $linha_user['id'];
                $nome = $linha_user['nome_completo'];

                echo "<option value='$id'>$nome</option>";

            }               
                                                        ?>

                                                        <option> </option>
                                                        <?php

            $query_user = mysqli_query($con,"SELECT * FROM usuarioz WHERE tipo = 'user' AND numero_processo = '' AND nome_completo != '' ORDER BY  id DESC");
            while($linha_user=mysqli_fetch_array($query_user)){

                $id = $linha_user['id'];
                $nome = $linha_user['nome_completo'];

                echo "<option value='$id'>$nome</option>";

            }               
                                                        ?>



                                                    </select>
                                                </div>



                                                <div class="form-group">
                                                    <label>Número do processo</label>
                                                    <input class="form-control" name="numero" value="<?php echo "$numero" ?>" style="width: 200px" />
                                                </div>

                                                <div class="form-group">
                                                    <label>Comarca</label>
                                                    <input class="form-control" name="comarca" value="<?php echo "$comarca" ?>" style="width: 200px" />
                                                </div>

                                                <div class="form-group">
                                                    <label>Parte Adversa</label>
                                                    <input class="form-control" name="parte_adversa" value="<?php echo "$parte_adversa" ?>" style="width: 400px" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Valor final acordado</label>
                                                    <input class="form-control preco_mask" name="valor" value="<?php echo "$valorfinal" ?>"  style="width: 120px" />
                                                </div>

                                                <div class="form-group">
                                                    <label>Resultado do processo</label>
                                                    <input class="form-control" name="resultado" value="<?php echo "$resultado" ?>"  />
                                                </div>

                                                <div class="form-group hide">
                                                    <label>Audiência</label>
                                                    <input type='text' class="form-control datepicker-here" name="audiencia" value="<?php echo "$data" ?>" data-language='pt-BR'  data-position="bottom left"  style="width: 120px" />

                                                </div>




                                                <div class="form-group">
                                                    <label>Detalhes adicionais</label>
                                                    <textarea class="form-control" name="texto" rows="6"><?php echo "$texto" ?></textarea>
                                                </div>



                                                <input type="hidden" name="data_registro" value="<?php echo "$hjSQL" ?>" >

                                                <input type="hidden" name="situacao" value="<?php echo "$situacao" ?>" >
                                                <input type="hidden" name="pagina" value="processos_concluidos" >
                                                <input type="hidden" name="tipo" value="fechado" >
                                                <input type="hidden" name="id" value="<?php echo "$u" ?>" >

                                                <button type="submit" class="btn btn-default">Editar</button>


                                                <!-------------------------------------->


                                            </form>

                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Form Elements -->
                        </div>

                        <?php } ?>

                        <div class="col-md-12">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-users" aria-hidden="true"></i> Usuários
                                </div>
                                <div class="panel-body">



                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th  style="width: 120px">N° Processo </th>
                                                    <th>Cliente</th>                                                    
                                                    <th  style="width: 90px">Comarca </th>
                                                    <th  style="width: 120px">Parte Adversa</th>
                                                    <th  style="width: 90px">Valor final</th>
                                                    <th  style="width: 90px">Situação</th>
                                                    <th  style="width: 50px">Editar</th>

                                                </tr>

                                            </thead>
                                            <tbody>

                                                <?php

                                                $action = "inserir";                                            

                                                $query = mysqli_query($con,"SELECT * FROM processos WHERE id_usuario != '' AND tipo ='fechado' ");    
                                                while($linha=mysqli_fetch_array($query)){

                                                    $id = $linha['id'];

                                                    $id_usuario = $linha['id_usuario'];
                                                    $parte_adversa = $linha['parte_adversa'];
                                                    $comarca = $linha['comarca'];
                                                    $situacao = $linha['situacao'];
                                                    $valorfinal= $linha['valorfinal'];
                                                    $numero = $linha['numero'];
                                                    $texto = $linha['texto'];                                                

                                                    $data = implode('/', array_reverse(explode('-', $linha['audiencia'])));
                                                    if($data == "00/00/0000") {$data = date("d/m/Y");}

                                                    $query_user = mysqli_query($con,"SELECT * FROM usuarioz WHERE id = '$id_usuario' ");
                                                    $linha_user=mysqli_fetch_array($query_user);

                                                    $nome = $linha_user['nome_completo'];





                                                ?>

                                                <tr class="">
                                                    <td><?php echo "$numero" ?></td>
                                                    <td><?php echo "$nome" ?></td>
                                                    <td><?php echo "$comarca" ?></td>
                                                    <td><?php echo "$parte_adversa" ?></td>
                                                    <td><?php echo "$valorfinal" ?></td>
                                                    <td><?php echo "$situacao" ?></td>

                                                    <td>
                                                        <a href="?u=<?php echo "$id" ?>&t=conclusao" class="btn btn-success btn-xs">Editar</a>


                                                    </td>
                                                </tr>

                                                <!-- Modal -->
                                                <div class="modal fade" id="myModal<?php echo "$id" ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                ...
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                <button type="button" class="btn btn-primary">Save changes</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>



                                                <?php } ?>













                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>





                </div>
                <!-- /. PAGE INNER  -->
            </div>
            <!-- /. PAGE INNER  -->
        </div>






    </body>
</html>
