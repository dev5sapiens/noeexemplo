<?php
include("includes/header_pdf.php");


$html = "



<h3 style='text-align: center'><strong>PROCURAÇÃO</strong><br><small style='font-weight: normal'>(ad judicia et extra)</small></h3>
<p></p>

<div class='bloko tam12'>
    <p>
        A procuração com cláusula 'ad judicia' confere ao advogado poderes amplos para todos os atos do processo judicial para o qual foi contratado, entre eles os recursos de toda natureza e incidentes processuais oriundos de uma mesma causa, em diversas instâncias, tribunais e órgãos. O que significa afirmar que encerrado o processo, pelo transito em julgado da sentença ou substabelecidos sem reservas, encerra os poderes outorgados, não podendo o advogado praticar com ela mais nenhum ato. 
    </p>
</div>
<p class='direita'>OUTORGANTE (S):</p>

<div class='bloko tam12'>
    <p>
        <strong style='text-transform: uppercase'>$nome_completo</strong>, Cpf. $cpf, Id. $rg., brasileiro(a), $profissao, domiciliado em Jaboticatubas/Mg., à Rua José Margarida, 200, bairro São Sebastião, Cep. 35.830-000.
    </p>
</div>
<p class='direita'>OUTORGADO (S):</p>




<div class='bloko tam12'>
    <p>
        <strong>ANDRÉ LUIZ RODRIGUES DE ASSIS</strong>, Oab/Mg. 140.218, Cpf. 089.309.406-41, brasileiro, solteiro, advogado.
    </p>
</div>
<p class='direita'>ENDEREÇO (S) DO (S) OUTORGADO (S):</p>

<div class='bloko tam12'>
    <p>
        <strong>Rua Benedito Quintino, 146, centro, Cep.35.830.000, Jaboticatubas/Mg. Tel. 36831600. Email: andre@andrerodriguesadv.com.br</strong>
    </p>
</div>

<p class='direita'>PODERES:</p>

<div class='bloko tam12'>
    <p>
        Pelo presente instrumento particular de procuração o (s) acima nomeado (s) outorgante (s), nomeia (m) e constitui (em) seu (s) bastante (s) procurador (es) os advogados também acima nomeados, outorgando-lhe (s) os poderes das cláusulas <strong>ad judicia et extra</strong>, para o foro em geral, conforme art. 105 do Cód. Proc. Civil, exceto receber citação, podendo o (s) dito (s), procurador (es) praticar (em) em conjunto ou separadamente, todos os atos reputados necessários ao fiel cumprimento do presente mandato, entre os quais confessar, reconhecer a procedência do pedido, renunciar aos direitos sobre os quais se fundam a ação para a qual foi outorgado este mandato, propor, contestar pretensões deduzidas em juízo contra o (s, a, as) outorgante (s), variar, requerer, transigir, desistir de ações, recorrer para qualquer instância, tribunal ou foro, receber e dar quitação, firmar compromissos em geral e de inventariante, requerer a instauração de correições, interpor incidentes processuais, fazer acordos, receber valores, dar e receber quitação, receber e dar quitação, levantar ou receber RPV e ALVARÁS, pedir a justiça gratuita e assinar declaração de hipossuficiência econômica, substabelecer estes poderes com ou sem reservas, de que tudo dará (ão) por firme e valioso. 
    </p>
    <div style='text-align:center'>
        <p></p>
        <p></p>


        <p>Jaboticatubas, $hoje_extenso.<br><br>
            ______________________________________</p>
        <p style='text-transform: uppercase'>$nome_completo</p>
        <p></p>
        <p></p>
        <p></p>

    </div>

</div>


";

$mpdf=new mPDF();
$mpdf->SetTitle('Procuração');
$stylesheet = file_get_contents('css/mpdf.css'); // external css
$mpdf->WriteHTML($stylesheet,1);
$mpdf->WriteHTML($html,2);

$mpdf->Output('Procuração.pdf', 'I');


exit;
?>