<?php include("includes/php/restrito.php") ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include("includes/header.php") ?>
</head>
<body>
    <div id="wrapper">
     <?php include("includes/topo.php") ?>
           <!-- /. NAV TOP  -->
               <?php include("includes/navbar.php") ?>
       <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>Chamados</h2>   
                       
                    </div>
                </div>
                 <!-- /. ROW  -->
                
                <div class="row">
                <div class="col-md-12">
                    <!-- Form Elements -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i></i> Chamados enviados
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Responder Chamados</h3>
                                    
                                    <?php 
                                    // Item multiplo textual
                                    $last = 0;
                                    $query = mysqli_query($con,"SELECT * FROM chamados WHERE texto02 = '' AND id_user !='' ORDER BY data_envio DESC");
                                    while ($linha=mysqli_fetch_array($query)) { 

                                    $id =$linha['id'];
                                    $id_user =$linha['id_user'];
                                    $data_envio =$linha['data_envio'];
                                    $assunto =$linha['assunto'];
                                    $texto1 =$linha['texto01'];
                                        
                                    $data_envio = implode('/', array_reverse(explode('-', $linha['data_envio'])));
                                        
                                    // Item único titular
                                    $query_user = mysqli_query($con,"SELECT * FROM usuarioz WHERE id = '$id_user'  ");
                                    $linha_user=mysqli_fetch_array($query_user);

                                    $nome =$linha_user['nome'];
                                    $numero_processo =$linha_user['numero_processo'];
     

                                    ?> 
                                    
                                       <div class="alert alert-info">
                                           
                                  
                                                 
                                    
                                    <form role="form" method="post" action="scriptz/gerar_chamado.php">
                                        
                                     
                                
                                <h3><strong>Assunto: <?php echo "$assunto"?></strong><br></h3>
                                        <ul>
                                            <li>Nome do usuário: <?php echo "$nome" ?> -  <a target="_blank" href="perfil_user.php?u=<?php echo "$id_user" ?>" class="btn btn-primary btn-xs">Ver perfil</a> </li>
                                            <li>N. Processo: <?php echo "$numero_processo" ?></li>
                                            <li>Data: <?php echo "$data_envio" ?></li>
                                        </ul>
                                        <p>
                                        <?php
                                  echo nl2br("$texto1");
                                        ?>
                                </p>
                                
                                 
                           
                                        
                                        
                                        
                                         <div class="form-group">
                                            <label>Resposta:</label>
                                            <textarea class="form-control" name="texto02" rows="5"></textarea>
                                             
                                             <input type="hidden" name="id_chamado" value="<?php echo "$id"?>" >
                                             <input type="hidden" name="data_resposta" value="<?php echo "$hjSQL"?>" >
                                             <input type="hidden" name="action" value="resposta" >
                                             
                                             
                                        </div>
                                        <button type="submit" class="btn btn-default">Enviar resposta</button>
                                        <!--- Dados demais ----------------------------------->
                                        

                                    </form>
                                            </div>
<?php }  ?> 
                                 
    </div>
                                
                            </div>
                        </div>
                    </div>
                     <!-- End Form Elements -->
                </div>
            </div>
           
         
           

                
                
    </div>
             <!-- /. PAGE INNER  -->
            </div>
             <!-- /. PAGE INNER  -->
            </div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Mensagem para o usuário</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form role="form">
                                     <div class="form-group">
                                            <label>Seu documento foi recusado pois</label>
                                            <textarea class="form-control" rows="3" autofocus>
                                            
                                         </textarea>
                                        </div>
                                        </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                            <button type="button" class="btn btn-primary">Enviar mensagem</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
    
   
</body>
</html>
