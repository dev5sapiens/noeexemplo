<?php

include("includes/header_pdf.php");




$html = "

<h3 style='text-align: center'><strong>DECLARAÇÃO DE POBREZA</strong><br><small style='font-weight: normal'>(para fins judiciais)</small></h3>
<p></p>
<p>$nome_completo, Cpf. $cpf, Id. $rg, nos termos da Lei 7.115 de 29.08.93, combinada com a Lei 1.060 de 05.02.50 e suas modificações e inciso LXXIV do art. 5º da Constituição da República Federativa do Brasil, sujeitando-se as sanções cíveis administrativas e criminais, previstas na legislação aplicável, DECLARA (m) ser (em) pobre (s)  no sentido legal, não tendo condições de custear o feito ora proposto.
</p>
<p>Requeiro, ainda, que o benefício abranja a todos os atos do processo, na forma do art. 98 do Código de Processo Civil.
</p>
<p></p>
<p></p>

<div style='text-align:center'>


<p>Jaboticatubas, $hoje_extenso.<br><br>
<p class='maior'>
______________________________________<br>
$nome_completo</p>


</div>


";

$mpdf->SetTitle('Declaração de Hiposuficiência');
$mpdf->WriteHTML($html);


$mpdf->Output('Declaração de Hiposuficiência.pdf', 'I');
exit;

?>