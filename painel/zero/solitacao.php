<?php include("includes/php/restrito.php") ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php include("includes/header.php") ?>
    </head>
    <body>
        <div id="wrapper">
            <?php include("includes/topo.php") ?>
            <!-- /. NAV TOP  -->
            <?php include("includes/navbar.php") ?>
            <div id="page-wrapper" >
                <div id="page-inner">

                    <div class="row">

                        <?php 
    // Item multiplo textual
    $last = 0;
$query = mysqli_query($con,"SELECT * FROM usuarioz WHERE id = '$i' AND status = 'pendente' ORDER BY  id DESC");
while ($linha=mysqli_fetch_array($query)) { 

    $id =$linha['id'];
    $usuario =$linha['usuario'];
    $telefones =$linha['telefones'];
    $motivo =$linha['motivo'];
    $email =$linha['usuario'];
    $texto01 =$linha['texto01'];
    $texto02 =$linha['texto02'];
    $fraude =$linha['fraude'];
    $consultou =$linha['consultou'];
    $empresa_reclamada =$linha['empresa_reclamada'];
    $cnpj =$linha['cnpj'];
    $endereco =$linha['endereco'];
    $razao =$linha['razao'];


}

                        ?>
                        <div class="col-md-12">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-users" aria-hidden="true"></i> Novos usuários

                                </div>
                                <div class="panel-body">

                                    <h3>Dados da solicitação</h3>
                                    <p>
                                        <strong>Telefones de contato:</strong> <?php echo "$telefones"?> <br>
                                        <strong>Email:</strong>  <?php echo "$email"?>br<br>
                                        <strong>Motivo da consulta:</strong>  <?php echo "$motivo"?>  <br>
                                        <strong>Vítima de fraude:</strong>  <?php echo "$fraude"?>  <br>
                                        <strong>Consulto SPC/SERASA:</strong>  <?php echo "$consultou"?>  
                                    </p>
                                    <P>
                                        <strong>Outros:</strong><br>
                                        <?php echo "$texto01"?></P>
                                    <P>
                                        <strong>Detalhes:</strong><br>
                                        <?php echo "$texto02"?></P>

                                    <hr>

                                    <h3>Dados da empresa reclamada</h3>
                                    <p>
                                        <strong>Empresa:</strong>  <?php echo "$empresa_reclamada"?>  <br>
                                        <strong>Razão social:</strong>  <?php echo "$razao"?>  <br>
                                        <strong>CNPJ:</strong>  <?php echo "$cnpj"?>  <br>
                                        <strong>Endereço:</strong>  <?php echo "$endereco"?>  
                                    </p>

                                    <hr>


                                    <!----------------------------->
                                    <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs" style="margin-top: 30px"> 
                                        <ul class="nav nav-tabs" id="myTabs" role="tablist">
                                            <li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">Aceitar solicitação</a></li> 
                                            <li role="presentation" class=""><a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile" aria-expanded="false">Recusar solicitação</a></li> 
                                            <li role="presentation" class="dropdown hide"> <a href="#" class="dropdown-toggle" id="myTabDrop1" data-toggle="dropdown" aria-controls="myTabDrop1-contents" aria-expanded="false">Dropdown <span class="caret"></span></a> 
                                                <ul class="dropdown-menu" aria-labelledby="myTabDrop1" id="myTabDrop1-contents"> 
                                                    <li><a href="#dropdown1" role="tab" id="dropdown1-tab" data-toggle="tab" aria-controls="dropdown1">@fat</a></li> 
                                                    <li><a href="#dropdown2" role="tab" id="dropdown2-tab" data-toggle="tab" aria-controls="dropdown2">@mdo</a></li> 
                                                </ul> 
                                            </li> 
                                        </ul> 

                                        <div class="tab-content" id="myTabContent"> 
                                            <div class="tab-pane fade active in" role="tabpanel" id="home" aria-labelledby="home-tab"> 
                                                <form role="form" method="post" action="scriptz/aceite.php" style="margin-top: 30px">
                                                    <div class="form-group">
                                                        <label>Mensagem de aceite da solicitação</label>
                                                        
                                                        <?php ?>
                                                        <textarea class="form-control" name="texto" rows="6">
Prezado(a)
Sua solicitação foi aceita pelo nosso escritório e daremos prosseguimento ao seu caso.

                                                        </textarea>
                                                    </div>


                                                    <input type="hidden" name="i" value="<?php echo "$id"?>">
                                                    <input type="hidden" name="r" value="s">
                                                    <button type="submit" class="btn btn-success">Aceitar solicitação</button>
                                                </form>
                                            </div> 


                                            <div class="tab-pane fade" role="tabpanel" id="profile" aria-labelledby="profile-tab"> 
                                                <form role="form" method="post" action="scriptz/aceite.php" style="margin-top: 30px">
                                                    <div class="form-group">
                                                        <label>Mensagem de recusa da solicitação</label>
                                                        <textarea class="form-control" name="texto" rows="6"></textarea>
                                                    </div>


                                                    <input type="hidden" name="i" value="<?php echo "$id"?>">
                                                    <input type="hidden" name="r" value="n">
                                                    <button type="submit" class="btn btn-danger">Recusar solicitação</button>
                                                </form>
                                            </div> 


                                        </div> 
                                    </div>
                                    <!----------------------------->

                                </div>
                            </div>

                        </div>
                    </div>






                </div>
                <!-- /. PAGE INNER  -->
            </div>
            <!-- /. PAGE INNER  -->
        </div>

    </body>
</html>
