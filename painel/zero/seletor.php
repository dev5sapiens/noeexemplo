
<script
  src="https://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>
<link href="dist/css/datepicker.min.css" rel="stylesheet" type="text/css">
<script src="dist/js/datepicker.min.js"></script>
<script src="dist/js/datepicker.pt-BR.js"></script>

<!-- Include English language -->
<script src="dist/js/i18n/datepicker.en.js"></script>


<input type='text' class="datepicker-here" data-language='pt-BR'  data-position="right top" />
