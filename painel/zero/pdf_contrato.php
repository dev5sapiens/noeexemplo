<?php
include("includes/header_pdf.php");




$header = '
<table width="100%" style="border-bottom: 1px solid #000000; vertical-align: bottom; font-family: serif; font-size: 9pt; color: #000088;"><tr>

<td width="100%" align="left"><img src="img/logo_adv.png" width="30%" /></td>
</tr></table>
';

$headerE = '
<table width="100%" style="border-bottom: 1px solid #000000; vertical-align: bottom; font-family: serif; font-size: 9pt; color: #000088;"><tr>

<td width="100%" align="left"><img src="img/logo_adv.png" width="30%" /></td>
</tr></table>
';


$footer = '<div align="center"><img src="img/rodape.png" style="width:800px" /></div>';
$footerE = '<div align="center"><img src="img/rodape.png" style="width:800px" /></div>';


$mpdf->SetHTMLHeader($header);
$mpdf->SetHTMLHeader($headerE,'E');
$mpdf->SetHTMLFooter($footer);
$mpdf->SetHTMLFooter($footerE,'E');


$html = "
<p><strong>CONTRATO DE PRESTAÇÃO DE SERVIÇOS E HONORÁRIOS ADVOCATÍCIOS</strong></p>
<p>Por este instrumento particular, de um lado, como advogados o Senhor <strong>André Luiz Rodrigues de Assis</strong>, Oab/Mg 140.218, com escritório situado em Jaboticatubas/Mg, à Rua Benedito Quintino, 146, centro, Jaboticatubas, Cep. 35.830-000, sócio da Resolva Rápido, e de outro lado, como contratante, <span class='maior'>$nome_completo<span>, Cpf. $nome_completo, Id. $rg, brasileiro(a), $profissao, $estado_civil, domiciliado em $endereco, tem entre si justo e contratado a prestação de serviços de advocacia mediante as cláusulas e condições seguintes:</p>
<p>1.º . O(s) advogado(s) compromete(m)-se, em cumprimento de mandato hoje outorgado pelo(s) cliente(s) acima descrito, para patrocinar Ação Judicial de Reparação por Danos Materiais/ Morais/ Obrigação de fazer, ou Propositura de Procedimento Extrajudicial de Ressarcimento de Danos, em desfavor da empresa <span class='maior'> $empresa_reclamada<span>.
</p>
<p>2.º. Em remuneração, o(s) cliente(s) pagará(ão) ao(s) Advogado(s) s os honorários no valor de 25% (vinte e cinco por cento) do valor do proveito que for obtido pelo contratante a serem pagos quando do efetivo recebimento, ficando o advogado autorizado a decotar o valor quando do levantamento/ recebimento do alvará, ou quando do depósito direto à conta do advogado contratado, ficando desde já o advogado contratado, autorizado a receber valores diretamente à conta indicada pelo mesmo.
</p>
<p>3º. Ao contratante, caberá o pagamento das custas processuais e demais despesas que forem necessárias ao bom andamento da ação, bem como o fornecimento de documentos e informações que os contratados solicitarem.
</p>
<p>4º. O contratante, desde já, indica que o depósito dos valores auferidos poderão ser depositados à conta NOME DO BANCO, CONTA CORRENTE/POUPANÇA (um ou outro) Nº ____________, AGÊNCIA: ______________, de titularidade de NOME DO TITULAR DA CONTA, CPF: ________________________________, sendo de inteira responsabilidade do contratante a veracidade dos dados bancário acima indicados.
</p>
<p>4º. Fica estabelecido que, iniciados os serviços especificados na cláusula primeira, são devidos os honorários contratados por completo neste instrumento, ainda que em caso de desistência por parte da contratante, ou se for cassado o mandato do contratado sem sua culpa, ou ainda, por acordo do contratante com a parte contrária, sem a devida aquiescência do contratado, podendo esse exigir os honorários de imediato.
</p>
<p>5ª. Fica estabelecido que os honorários contratados, cobrem, apenas os serviços prestados em primeira instância, na Comarca competente do domicílio do contratante, 
</p>
<p>6ª. É de inteira responsabilidade do contratante a veracidade e a lisura dos fatos narrados na plataforma online www.resolvarapido.com, ou nos outros canais de atendimento da empresa, respondendo o mesmo por todas as consequências administrativas, cíveis e criminais das declarações fáticas. </p>

<p>7ª. Os honorários só serão devidos no caso de êxito da ação patrocinada pelo Contratado. No caso de insucesso da demanda o Contratante nada terá de honorários a pagar.
</p>
<p>8ª. Os honorários de condenação (sucumbência), se houver, pertencerão ao Advogado, sem exclusão dos que ora são contratados, de conformidade com os artigos 23 da Lei nº 8.906/94 e 35, parágrafo 1º,  do Código de Ética e Disciplina da Ordem dos Advogado do Brasil.
</p>
<p>9º. Elegem as partes o foro da comarca de Belo Horizonte/Mg, para dirimir dúvidas sobre este contrato, podendo ainda o(s) Advogado(s), em caso de execução, optar pelo foro da residência do(a) cliente.
</p>
<p>E por estarem assim justos e contratados, firmam o presente, em duas vias, perante as testemunhas abaixo, para que produzam todos seus efeitos legais e de direito.
</p>
<div style='text-align:center'>
<p></p>
<p></p>

<p>Belo Horizonte, $hoje_extenso.<br><br>
______________________________________</p>
<p>André Luiz Rodrigues de Assis<br>Oab/Mg. 140.218</p>
<p></p>
<p></p>
<p>___________________________________<br>
$nome_completo</p>

</div>


";

$mpdf->WriteHTML($html);

$mpdf->Output('Contrato.pdf', 'I');
exit;

?>