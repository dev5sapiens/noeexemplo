<?php include("includes/php/restrito.php") ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include("includes/header.php") ?>
</head>
<body>
    <div id="wrapper">
     <?php include("includes/topo.php") ?>
           <!-- /. NAV TOP  -->
               <?php include("includes/navbar.php") ?>
       <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>Abrir chamados</h2>   
                       
                    </div>
                </div>
                 <!-- /. ROW  -->
               
                <div class="row">
                <div class="col-md-12">
                    <!-- Form Elements -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i></i> Chamados
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Enviar chamado</h3>
                                    <form role="form" method="post" action="scriptz/gerar_chamado.php">
                                        
                                        
                                         <div class="form-group">
                                            <label>Assunto</label>
                                            <select name="assunto" class="form-control">
                                                <option>Documentos</option>
                                                <option>Dúvidas do processo</option>
                                                <option>Outro</option>

                                            </select>
                                        </div>
                                         <div class="form-group">
                                            <label>Mensagem</label>
                                            <textarea name="texto01" class="form-control" rows="5"></textarea>
                                             
                                             <input type="hidden" name="id_user" value="<?php echo "$id_user"?>" >
                                             <input type="hidden" name="data_envio" value="<?php echo "$hjSQL"?>" >
                                             <input type="hidden" name="action" value="pergunta" >
                                             
                                             
                                        </div>
                                        <button type="submit" class="btn btn-default">Abrir chamado</button>
                                        <!--- Dados demais ----------------------------------->
                                        

                                    </form>

                                 
    </div>
                                
                            </div>
                        </div>
                    </div>
                     <!-- End Form Elements -->
                </div>
            </div>
                
                 
                <div class="row">
               
                   
                   <div class="col-md-12">
               
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           <i class="fa fa-file-text-o" aria-hidden="true"></i></i> Tabela
                        </div>
                        <div class="panel-body">
                            
                            <h3>Histórico de chamados</h3>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                     
                <th>Tabela de chamados</th>
               
                                            <th  style="width: 100px"></th>

                                        </tr>
                                        
                                    </thead>
                                    <tbody>
                                        
                                                       <?php 
    // Item multiplo +  Galeria + Last
    $last = 0;
    $query = mysqli_query($con,"SELECT * FROM chamados WHERE id_user='$id_user' ORDER BY  id DESC");
    while ($linha=mysqli_fetch_array($query)) { 
                    
    $id =$linha['id'];
    $assunto =$linha['assunto'];
    $texto02 =$linha['texto02'];

    $data_envio = implode('/', array_reverse(explode('-', $linha['data_envio'])));

        $resposta = "warning";
        $status = "Aguardando";
        
        if($texto02 != ""){
            $resposta = "success";
            $status = "Respondido"; 
        }
        
        
            
 ?>
                                        
                                         <tr>
                                            <td><?php echo"$data_envio - $assunto"?></td>
                                            <td>
                                                <span class="label label-<?php echo "$resposta"?>"><?php echo "$status"?></span>
                                               
                                            </td>
                                        </tr>
                
                     <?php  
          
               }
                ?>
                                        
                                       
                                        
                                        
                       
                                     
                    
     
                                                                               
                                 
                                                                         
                                                                                 
                                       

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    
                    </div>
            </div>    
           

                
                
    </div>
             <!-- /. PAGE INNER  -->
            </div>
             <!-- /. PAGE INNER  -->
            </div>

    
   
</body>
</html>
