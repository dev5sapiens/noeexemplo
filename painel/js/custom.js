


/*=============================================================
    Authour URI: www.binarycart.com
    License: Commons Attribution 3.0

    http://creativecommons.org/licenses/by/3.0/

    100% To use For Personal And Commercial Use.
    IN EXCHANGE JUST GIVE US CREDITS AND TELL YOUR FRIENDS ABOUT US

    ========================================================  */


(function ($) {
    "use strict";
    var mainApp = {

        main_fun: function () {
            /*====================================
            METIS MENU 
            ======================================*/
            $('#main-menu').metisMenu();

            /*====================================
              LOAD APPROPRIATE MENU BAR
           ======================================*/
            $(window).bind("load resize", function () {
                if ($(this).width() < 768) {
                    $('div.sidebar-collapse').addClass('collapse')
                } else {
                    $('div.sidebar-collapse').removeClass('collapse')
                }
            });

            /*====================================
            MORRIS BAR CHART
         ======================================*/






        },

        initialization: function () {
            mainApp.main_fun();

        }

    }
    // Initializing ///

    $(document).ready(function () {
        mainApp.main_fun();
    });


    $(function() {
        $('.cf7-employees').mask('000');
        $('.cf7-date').mask('00/00/0000');
        $('.cf7-mmyyyy').mask('00/0000');
        $('.cf7-time').mask('00:00:00');
        $('.cf7-ein').mask('00-0000000');
        $('.cf7-money').mask('000.000,00', {reverse: true});
        $('.cf7-percent').mask('##0%', {reverse: true});
        $('.cf7-phone').mask('(000) 000-0000');
        $('.cf7-whats').mask('(00) 0000-0000');
       
        $('.cf7-ssn').mask('000-00-0000');
        $('.cf7-zip').mask('00000');
        $(".cf7-uppercase").on('input', function(evt) {
            var input = $(this);
            var start = input[0].selectionStart;
            $(this).val(function (_, val) {
                return val.toUpperCase();
            });
            input[0].selectionStart = input[0].selectionEnd = start;
        });
        $.fn.capitalize = function () {
            $.each(this, function () {
                var split = this.value.split(' ');
                for (var i = 0, len = split.length; i < len; i++) {
                    split[i] = split[i].charAt(0).toUpperCase() + split[i].slice(1);
                }
                this.value = split.join(' ');
            });
            return this;
        };
        $('.cf7-capitalize').on('keyup', function () {
            $(this).capitalize();
        }).capitalize();
    });
    
    

}(jQuery));


