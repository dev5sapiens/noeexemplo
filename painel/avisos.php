<?php include("includes/php/restrito.php") ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php include("includes/header.php") ?>






    </head>
    <body>
        <div id="wrapper">
            <?php include("includes/topo.php") ?>
            <!-- /. NAV TOP  -->
            <?php include("includes/navbar.php") ?>
            <div id="page-wrapper" >
                <div id="page-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Avisos</h2>   

                        </div>
                    </div>
                    <!-- /. ROW  -->

                    <div class="row">
                        <!--- Formulário --------------------------------------------------------->
                        <div class="col-md-12">
                            <!-- Form Elements -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Avisos
                                </div>

                                <?php
    $action = "inserir";

$titl = "Inserir";

if(isset($s)){

    $query = mysqli_query($con,"SELECT * FROM avisos WHERE id = '$s' ");
    $action = "editar";


    $titl = "Editar ";
} else{


    $query = mysqli_query($con,"SELECT * FROM avisos WHERE nome = '' ");
    $action = "inserir";

}





$linha=mysqli_fetch_array($query);

$id = $linha['id'];
$nome = $linha['nome'];

$inicio = $linha['inicio'];
$fim = $linha['fim'];
$visibilidade = $linha['visibilidade'];
$texto = $linha['texto'];


$inicio =  setardata($inicio, "x");
$fim =  setardata($fim, "x");

$setores = "Master,Médico associado,Médico não-associado,Coordenador,Supervisor,Colaborador";


if( strpos( $visibilidade, "Master" ) !== false ) {$marcado1 = "checked";}





                                ?>
                                <div class="panel-body">
                                    <div class="row">

                                        <div class="col-md-12">




                                            <h3>Inserir aviso </h3>

                                            <form role="form" method="post" action="scriptz01/inserir_aviso.php">

                                                <div class="form-group">
                                                    <label>Título</label>
                                                    <input class="form-control" required="" name="nome" value="<?php echo "$nome" ?>">
                                                </div>

                                                <div class="form-group total left">
                                                    <label class="total left">Exibição</label>
                                                    <input type="text" class="form-control left datepicker-here" name="inicio" value="<?php echo "$inicio" ?>" data-language="pt-BR" data-position="bottom left" style="width: 120px">
                                                    <span class="left" style="    margin: 6px;">até</span>
                                                    <input type="text" class="form-control left datepicker-here" name="fim" value="<?php echo "$fim" ?>" data-language="pt-BR" data-position="bottom left" style="width: 120px">
                                                </div>



                                                <div class="form-group">
                                                    <label>Visível para:</label>

                                                    <div class="cks total left">

                                                        <?php



    $i = "";
                                                           $marcado = "";
                                                           $string_setores = explode(",", $setores);
                                                           foreach($string_setores as $elemento)
                                                           {
                                                               ++$i;
                                                               echo "<label class='ck-button'>
                                                            <input name='visibilidade[]' $marcado$i type='checkbox' value='$elemento'><span>$elemento</span>
                                                        </label>
                                                        ";
                                                           }

                                                        ?>



                                                        <!-- Dados ------------------------------>

<label class='ck-button'>
                                                            <input id="checkAll"  type='checkbox' ><span>Selecionar Todos</span>
                                                        </label>
                                                        <script>
                                                            $('#checkAll').click(function () {    
                                                                $('input:checkbox').prop('checked', this.checked);    
                                                            });
                                                        </script>









                                                    </div>



                                                    <!-- /Dados ----------------------------->

                                                </div>






                                                <div class="form-group">
                                                    <label>Texto</label>
                                                    <textarea class="form-control editorx02" name="texto" rows="6"><?php echo "$texto" ?></textarea>
                                                </div>



                                                <input type="hidden" name="action" value="<?php echo "$action" ?>">
                                                <input type="hidden" name="id" value="<?php echo "$id" ?>">
                                                <button type="submit" class="btn btn-default">inserir</button>


                                                <!-------------------------------------->


                                            </form>


                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Form Elements -->
                        </div>


                        <!---/Formulário --------------------------------------------------------->

                        <!--- Tabela --------------------------------------------------------->
                        <div class="col-md-12" id="tabela">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-file-text-o" aria-hidden="true"></i> Avisos
                                </div>
                                <div class="panel-body">



                                    <div class="table-responsive">
                                        <table class="table table-bordered" >
                                            <thead>
                                                <tr>
                                                    <th >Aviso</th>
                                                    <th style="width: 300px">Visível para</th>

                                                    <th  style="width: 110px"></th>

                                                </tr>

                                            </thead>
                                            <tbody>


                                                <?php 

    $query = mysqli_query($con,"SELECT * FROM avisos WHERE nome != '' ");

                                                        while($linha=mysqli_fetch_array($query)){

                                                            $id = $linha['id'];
                                                            $nome = $linha['nome'];
                                                            $visibilidade = $linha['visibilidade'];
                                                            
                                                            


                                                ?>
                                                <tr class="">
                                                    <td><?php echo "$nome"?></td>
                                                    <td><?php echo rtrim(str_replace(",", ", ", "$visibilidade"),", "); ?></td>

                                                    <td>
                                                        <a href="?s=<?php echo "$id" ?>" class="btn btn-success btn-xs">Editar</a>
                                                        <a href="scriptz/deletar.php?id=<?php echo "$id" ?>&tipo=aviso" class="btn btn-danger btn-xs" onclick="return confirm('Deseja realmente excluir este item?')" >Excluir</a>


                                                    </td>
                                                </tr>

                                                <?php }?>


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!---/Tabela --------------------------------------------------------->

                    </div>





                </div>
                <!-- /. PAGE INNER  -->
            </div>
            <!-- /. PAGE INNER  -->
        </div>






    </body>
</html>
