<?php 

date_default_timezone_set('America/Sao_Paulo');

include("includes/php/mysql_con.php");
include("includes/php/anti_sql_injection.php");

include("includes/funcoes/datas.php");



$hjSQL = date("Y-m-d");

foreach ($_REQUEST as $campo => $valor) { $$campo = $valor;}



$sessao = $_SESSION['s_login'];

//echo "$sessao<br>";

$arquivo = explode('-', $sessao);

$tipo_user = $arquivo[0];
$id_user = $arquivo[1];

//echo "<br>$tipo_user<br>";

?>



<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Painel de Controle</title>




<link href="css/bootstrap.css" rel="stylesheet" />
<link href="css/font-awesome.css" rel="stylesheet" />
<link href="css/custom.css" rel="stylesheet" />  
<link href="css/site.css" rel="stylesheet" />

<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700&amp;subset=latin-ext" rel="stylesheet">


<script src="js/jquery-1.10.2.js"></script>
<script src="js/bootstrap.min.js"></script>



<link href="dist/css/datepicker.min.css" rel="stylesheet" type="text/css">
<script src="dist/js/datepicker.min.js"></script>
<script src="dist/js/datepicker.pt-BR.js"></script>

<script src="js/jquery.metisMenu.js"></script>





<script type="text/javascript" src="https://d3a39i8rhcsf8w.cloudfront.net/js/jquery.mask.min.js"></script>
<script type="text/javascript" src="tinymce/tinymce.min.js" ></script>
<script>

    /*
    tinymcezzz.init({
        selector: '.editorx',  // change this value according to your HTML
        height: 500,
        toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',

        content_style: "p {color: black; font-size: 14px}"
    });
*/
    tinymce.init({
        selector: '.editorx',
        height: 200,
        content_style: "p {font-size: 16px}",
        menubar: false,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor textcolor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code help'
        ],
        toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat'
    });

    tinymce.init({
        selector: '.editorx02',
        height: 500,
        content_style: "p {font-size: 16px}",
        menubar: false,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor textcolor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code help'
        ],
        toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat'
    });



</script> 


<script src="js/custom.js"></script>


<script src="colorbox/colorbox.js"></script>
<script>
    $(document).ready(function(){
        //Examples of how to assign the Colorbox event to elements
        $(".group1").colorbox({rel:'group1'});
        $(".group2").colorbox({rel:'group2', transition:"fade"});
        $(".group3").colorbox({rel:'group3', transition:"none", width:"75%", height:"75%"});
        $(".group4").colorbox({rel:'group4', slideshow:true});
        $(".ajax").colorbox();
        $(".youtube").colorbox({iframe:true, innerWidth:640, innerHeight:390});
        $(".vimeo").colorbox({iframe:true, innerWidth:500, innerHeight:409});
        $(".iframe").colorbox({iframe:true, width:"800px", height:"600px", overlayClose: false });
        $(".iframe02").colorbox({iframe:true, width:"700px", height:"450px", overlayClose: false });
        $(".inline").colorbox({inline:true, width:"50%"});
        $(".callbacks").colorbox({
            onOpen:function(){ alert('onOpen: colorbox is about to open'); },
            onLoad:function(){ alert('onLoad: colorbox has started to load the targeted content'); },
            onComplete:function(){ alert('onComplete: colorbox has displayed the loaded content'); },
            onCleanup:function(){ alert('onCleanup: colorbox has begun the close process'); },
            onClosed:function(){ alert('onClosed: colorbox has completely closed'); }
        });

        $('.non-retina').colorbox({rel:'group5', transition:'none'})
        $('.retina').colorbox({rel:'group5', transition:'none', retinaImage:true, retinaUrl:true});

        //Example of preserving a JavaScript event for inline calls.
        $("#click").click(function(){ 
            $('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
            return false;
        });
    });
</script>


<link rel="stylesheet" href="colorbox/colorbox.css" />
<link rel="stylesheet" type="text/css" href="fbox/jquery.fancybox.css?v=2.1.5" media="screen" />
<script type="text/javascript" src="fbox/jquery.fancybox.js?v=2.1.5"></script>

<script type="text/javascript" src="fbox/fancyscripts.js"></script>


<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.css"/> 
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.js"></script>
<script>
    $(document).ready(function() {
        $('#data_tabela').DataTable();
    } );
</script>

