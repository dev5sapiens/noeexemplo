<?php
/*
* Anti Injection
* Verifica e Trata as informações
* Autor: Danilo Iannone - danilowm@gmail.com
*/

function anti_injection( $obj ) {
   $obj = preg_replace("/(from|alter table|select|insert|delete|update|where|drop table|show tables|#|*|--|\\)/i", "", $obj);
   $obj = trim($obj);
   $obj = strip_tags($obj);
   if(!get_magic_quotes_gpc()) {
      $obj = addslashes($obj);
      return $obj;
   }}
?>