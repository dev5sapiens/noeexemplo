<?php

$paginar = mysql_query("SELECT * FROM item ");

//////////////////////////////////// Adam's Pagination Logic ////////////////////////////////////////////////////////////////////////
$nr = mysql_num_rows($paginar); // Get total of Num rows from the database query
if (isset($_GET['pn'])) { // Get pn from URL vars if it is present
    $pagNum = preg_replace('#[^0-9]#i', '', $_GET['pn']); // filter everything but numbers for security(new)
    //$pagNum = ereg_replace("[^0-9]", "", $_GET['pn']); // filter everything but numbers for security(deprecated)
} else { // If the pn URL variable is not present force it to be value of page number 1
    $pagNum = 1;
} 
//This is where we set how many database items to show on each page 
$itemsPerPage = 7; 
// Get the value of the last page in the pagination result set
$lastPage = ceil($nr / $itemsPerPage);
// Be sure URL variable $pagNum(page number) is no lower than page 1 and no higher than $lastpage

// This creates the numbers to click in between the next and back buttons
// This section is explained well in the video that accompanies this script
$centerPages = "";
$sub1 = $pagNum - 1;
$sub2 = $pagNum - 2;
$add1 = $pagNum + 1;
$add2 = $pagNum + 2;
if ($pagNum == 1) {
    $centerPages .= '<span class="pagNumActive">' . $pagNum . '</span>';
    $centerPages .= '<a href="blog.php?pn=' . $add1 . '">' . $add1 . '</a>';
} else if ($pagNum == $lastPage) {
    $centerPages .= '<a href="blog.php?pn=' . $sub1 . '">' . $sub1 . '</a>';
    $centerPages .= '<span class="pagNumActive">' . $pagNum . '</span> &nbsp;';
} else if ($pagNum > 2 && $pagNum < ($lastPage - 1)) {
    $centerPages .= '<a href="blog.php?pn=' . $sub2 . '">' . $sub2 . '</a>';
    $centerPages .= '<a href="blog.php?pn=' . $sub1 . '">' . $sub1 . '</a>';
    $centerPages .= '<span class="pagNumActive">' . $pagNum . '</span>';
    $centerPages .= '<a href="blog.php?pn=' . $add1 . '">' . $add1 . '</a>';
    $centerPages .= '<a href="blog.php?pn=' . $add2 . '">' . $add2 . '</a>';
} else if ($pagNum > 1 && $pagNum < $lastPage) {
    $centerPages .= '<a href="blog.php?pn=' . $sub1 . '">' . $sub1 . '</a>';
    $centerPages .= '<span class="pagNumActive">' . $pagNum . '</span>';
    $centerPages .= '<a href="blog.php?pn=' . $add1 . '">' . $add1 . '</a>';
}
// This line sets the "LIMIT" range... the 2 values we place to choose a range of rows from database in our query
$limit = 'LIMIT ' .($pagNum - 1) * $itemsPerPage .',' .$itemsPerPage; 
// Now we are going to run the same query as above but this time add $limit onto the end of the SQL syntax
// $paginar2 is what we will use to fuel our while loop statement below

//////////////////////////////// END Adam's Pagination Logic ////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// Adam's Pagination Display Setup /////////////////////////////////////////////////////////////////////
$paginationDisplay = ""; // Initialize the pagination output variable
// This code runs only if the last page variable is ot equal to 1, if it is only 1 page we require no paginated links to display
if ($lastPage != "1"){
    
    // If we are not on page 1 we can place the Back button
    if ($pagNum != 1) {
        $previous = $pagNum - 1;
        $paginationDisplay .=  '<a href="blog.php?pn=' . $previous . '"> Anterior</a> ';
    } 
    // Lay in the clickable numbers display here between the Back and Next links
    $paginationDisplay .= '<span class="paginationNumbers">' . $centerPages . '</span>';
    // If we are not on the very last page we can place the Next button
    if ($pagNum != $lastPage) {
        $nextPage = $pagNum + 1;
        $paginationDisplay .=  '<a href="blog.php?pn=' . $nextPage . '"> Pr�xima</a> ';
    } 
}

?>