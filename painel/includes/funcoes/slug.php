<?php

function slugify($text){
    // replace non letter or digits by -
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);

    // transliterate
    //$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    //iconv removido por incompatibilidade no servidor
    $text = utf8_encode($text);

    //echo '<pre>'; print_r($text);die;

    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);

    // trim
    $text = trim($text, '-');

    // remove duplicated - symbols
    $text = preg_replace('~-+~', '-', $text);

    // lowercase
    $text = strtolower($text);

    if (empty($text)) {
      return 'n-a';
    }

    return $text;
}

//echo  slugify('Hello world, this is the name of my article ção');
// hello-world-this-is-the-name-of-my-article