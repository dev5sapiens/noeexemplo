-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 09/04/2018 às 17:03
-- Versão do servidor: 5.6.39
-- Versão do PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `nucleomg_painel`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `artigos`
--

CREATE TABLE `artigos` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `data` date NOT NULL,
  `texto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `artigos`
--

INSERT INTO `artigos` (`id`, `nome`, `slug`, `data`, `texto`) VALUES
(1, 'nome do artigo01', 'nome-do-artigo01', '2018-01-20', '<p>asdfasd asdf asdfasdf</p>'),
(2, 'outro com Ã£ e Ã§01', 'outro-com-a-e-c01', '2018-01-20', '<p>asdf asdf</p>'),
(4, 'asdf asd f', 'asdf-asd-f', '0000-00-00', '<p>asdf as dfasdf</p>'),
(5, 'asdf asd f', 'asdf-asd-f', '2018-01-19', '<p>asdf as dfasdf</p>'),
(7, '', '', '0000-00-00', '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `atividades`
--

CREATE TABLE `atividades` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `setor` int(11) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `responsavel` varchar(500) NOT NULL,
  `inicio` date NOT NULL,
  `fim` date NOT NULL,
  `status_admin` int(1) NOT NULL,
  `status_user` int(1) NOT NULL,
  `texto` text NOT NULL,
  `id_pratica` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `atividades`
--

INSERT INTO `atividades` (`id`, `nome`, `setor`, `slug`, `responsavel`, `inicio`, `fim`, `status_admin`, `status_user`, `texto`, `id_pratica`) VALUES
(98, '', 0, '', '1', '2018-04-08', '2018-04-14', 0, 0, '', 47),
(99, '', 0, '', '27', '2018-04-08', '2018-04-14', 0, 0, '', 47),
(100, '', 0, '', '28', '2018-04-08', '2018-04-14', 0, 0, '', 47);

-- --------------------------------------------------------

--
-- Estrutura para tabela `avisos`
--

CREATE TABLE `avisos` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `visibilidade` varchar(300) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `inicio` date NOT NULL,
  `fim` date NOT NULL,
  `texto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `avisos`
--

INSERT INTO `avisos` (`id`, `nome`, `visibilidade`, `slug`, `inicio`, `fim`, `texto`) VALUES
(1, 'sdfgfg', 'Master,Colaborador,', 'sdfgfg', '0000-00-00', '0000-00-00', '<p>asd</p>'),
(3, 'teste', 'MÃ©dico associado,', '', '2018-03-23', '2018-03-31', '<p>adf</p>'),
(4, 'teste', 'Master,', '', '2018-03-23', '2018-03-23', '<p>teste</p>'),
(5, 'teste', '', '', '2018-03-26', '2018-03-26', '<p>teste</p>'),
(6, 'buscar contra-cheque', '', '', '2018-03-29', '2018-03-29', '<p>teste</p>'),
(7, 'aviso', '', '', '2018-03-31', '2018-03-31', '<p>aviso</p>'),
(8, 'Cuidado', '', '', '2018-04-05', '2018-04-05', '<p>Cuidado</p>'),
(9, 'teste 1', '', '', '2018-04-05', '2018-04-05', '<p>teste 1</p>'),
(10, '', '', '', '0000-00-00', '0000-00-00', '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `beneficios`
--

CREATE TABLE `beneficios` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `texto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `beneficios`
--

INSERT INTO `beneficios` (`id`, `nome`, `slug`, `texto`) VALUES
(1, 'nome do artigo 01', 'nome-do-artigo-01', '<p>asdfasd asdf asdfasdf</p>'),
(2, 'outro com Ã£ e Ã§ 01', 'outro-com-a-e-c-01', '<p>asdf asdf</p>'),
(3, 'cliente', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla dapibus vitae tortor non ultrices. Phasellus non luctus nunc. Sed lacinia quam lacus, a feugiat velit congue sed. Aenean hendrerit enim sed dolor faucibus tempor. Ut pulvinar nisi id ex ornare tempor. Nulla scelerisque augue in tortor tristique ullamcorper in vitae erat. Duis aliquet magna at nisl aliquam maximus. In egestas dui vitae rutrum elementum. Suspendisse potenti. Integer pellentesque rutrum erat, a pharetra risus eleifend sed.</p>\r\n<p>Phasellus pellentesque, purus eget dignissim imperdiet, urna urna iaculis diam, a feugiat purus lectus eu ipsum. Suspendisse ac ipsum molestie, ullamcorper metus non, iaculis diam. Nam et porttitor felis. Integer tincidunt sapien purus, ac facilisis urna scelerisque ac. Sed sed urna felis. Nam blandit tristique mauris. Suspendisse a felis nec sapien volutpat gravida vel vel elit. Curabitur consequat imperdiet neque id lacinia. In malesuada sagittis nisl, vel scelerisque lacus tempus quis. Mauris quis lorem id ante ornare scelerisque. In nec porta nulla, vel scelerisque ligula. Donec at rhoncus nibh. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\r\n<p>Nullam in suscipit ante. Proin a turpis commodo, dictum diam at, pretium mi. Morbi vel tristique lorem, at fringilla elit. Maecenas ultrices eu nisi a tincidunt. Morbi lectus odio, scelerisque ut nibh vel, iaculis suscipit turpis. Donec vel diam non eros accumsan efficitur ac vitae lorem. Curabitur euismod nulla ipsum, vel lobortis sapien congue vitae. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla facilisi. Nam viverra, odio vitae rutrum ornare, dolor metus suscipit metus, non semper tortor mauris eget ligula. Quisque at euismod metus. Maecenas est sem, suscipit ut vulputate non, eleifend non tortor.</p>'),
(5, 'Outro artigo', 'outro-artigo', '<p>asdf adf</p>'),
(6, 'benefÃ­cio', 'beneficio', '<p>sdafasdf</p>'),
(7, '', '', '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `busca`
--

CREATE TABLE `busca` (
  `id` int(11) NOT NULL,
  `elemento` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `busca`
--

INSERT INTO `busca` (`id`, `elemento`) VALUES
(1, '2');

-- --------------------------------------------------------

--
-- Estrutura para tabela `carrinho`
--

CREATE TABLE `carrinho` (
  `id` int(11) NOT NULL,
  `cookie` varchar(100) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `data_solicitacao` date NOT NULL,
  `data_devolucao` date NOT NULL,
  `tamanho` varchar(100) NOT NULL,
  `qte` int(11) NOT NULL,
  `preco_unitario` decimal(11,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `carrinho`
--

INSERT INTO `carrinho` (`id`, `cookie`, `id_produto`, `data_solicitacao`, `data_devolucao`, `tamanho`, `qte`, `preco_unitario`) VALUES
(1, 'nnjmipktv8', 2, '2017-12-06', '2017-12-10', '35', 1, '12.00'),
(3, 'nnjmipktv8', 7, '2017-12-26', '2017-12-10', '7', 3, '250.00');

-- --------------------------------------------------------

--
-- Estrutura para tabela `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `tipo` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `categorias`
--

INSERT INTO `categorias` (`id`, `nome`, `tipo`) VALUES
(1, 'Vestido de Noiva', 'trabalho'),
(2, 'Vestido de Festas', 'trabalho'),
(3, 'Acessórios', 'trabalho');

-- --------------------------------------------------------

--
-- Estrutura para tabela `chamados`
--

CREATE TABLE `chamados` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `data_envio` date NOT NULL,
  `data_resposta` date NOT NULL,
  `assunto` varchar(20) NOT NULL,
  `texto01` text NOT NULL,
  `texto02` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `chamados`
--

INSERT INTO `chamados` (`id`, `id_user`, `data_envio`, `data_resposta`, `assunto`, `texto01`, `texto02`) VALUES
(1, 2, '2017-01-12', '2017-01-12', 'Documentos', 'texto da pergunta', 'asdfasd'),
(2, 2, '2017-01-25', '2017-01-25', 'DÃºvidas do processo', 'Texto da mensagem', 'asdfaf'),
(3, 2, '2017-01-25', '2017-05-31', 'DÃºvidas do processo', 'DÃºvidas geradas do processo', 'Teste'),
(4, 2, '2017-01-25', '2017-01-25', 'DÃºvidas do processo', 'DÃºvidas geradas do processo', 'asfdasfd'),
(5, 2, '2017-01-25', '2017-05-31', 'Documentos', 'Novo chamado final', 'Teste'),
(6, 2, '2017-01-25', '2017-05-31', 'Documentos', 'Novo chamado final final', 'Teste'),
(7, 2, '2017-05-10', '2017-05-31', 'DÃºvidas do processo', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin non semper leo. In eros arcu, pretium non gravida sit amet, faucibus ac tellus. Nulla facilisi. Donec sagittis, lorem eget vehicula aliquet, urna neque pharetra nisl, id laoreet purus ante eget dolor. Aenean pellentesque ex at libero lacinia, quis sollicitudin erat consequat. Phasellus posuere et sem ut rhoncus. Duis quam est, scelerisque eu varius at, rhoncus quis nulla. Nullam faucibus et elit vehicula vehicula. Maecenas consectetur diam a nisi mollis hendrerit. Cras consequat dignissim magna, eu auctor enim fringilla a. Nulla ultricies ultrices fringilla. In urna augue, lobortis non auctor in, ultricies nec est. Maecenas mollis tristique feugiat. Vestibulum vel lorem malesuada, iaculis purus vitae, vestibulum nulla.', 'Teste'),
(8, 23, '2017-07-19', '2017-07-19', 'DÃºvidas do processo', 'Estou com dÃºvidas', 'Respondido'),
(9, 9, '2017-08-10', '2017-08-22', 'DÃºvidas do processo', 'c\\zxc\\zczxc', 'Ã§ljlÃ§kj'),
(10, 20, '2017-08-21', '2017-08-22', 'Documentos', 'Nova mensagem para o administrador', 'oiquwroijsdfk jasdfsadjfu'),
(11, 0, '0000-00-00', '0000-00-00', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `controle`
--

CREATE TABLE `controle` (
  `id` int(11) NOT NULL,
  `id_locacao` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `dia` date NOT NULL,
  `qte` int(11) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `controle`
--

INSERT INTO `controle` (`id`, `id_locacao`, `id_produto`, `dia`, `qte`, `status`) VALUES
(14, 2, 22, '2017-12-20', 1, 'ok'),
(15, 2, 22, '2017-12-21', 1, 'ok'),
(16, 2, 22, '2017-12-22', 1, 'ok'),
(17, 2, 22, '2017-12-23', 1, 'ok'),
(34, 3, 23, '2017-12-20', 3, 'ok'),
(35, 3, 23, '2017-12-21', 3, 'ok'),
(36, 3, 23, '2017-12-22', 3, 'ok'),
(37, 3, 23, '2017-12-23', 3, 'ok'),
(38, 3, 23, '2017-12-24', 3, 'ok'),
(39, 3, 23, '2017-12-25', 3, 'ok'),
(40, 3, 23, '2017-12-26', 3, 'ok'),
(41, 3, 23, '2017-12-27', 3, 'ok'),
(42, 3, 22, '2017-12-20', 9, 'ok'),
(43, 3, 22, '2017-12-21', 9, 'ok'),
(44, 3, 22, '2017-12-22', 9, 'ok'),
(45, 3, 22, '2017-12-23', 9, 'ok'),
(46, 3, 22, '2017-12-24', 9, 'ok'),
(47, 3, 22, '2017-12-25', 9, 'ok'),
(48, 3, 22, '2017-12-26', 9, 'ok'),
(49, 3, 22, '2017-12-27', 9, 'ok');

-- --------------------------------------------------------

--
-- Estrutura para tabela `dados`
--

CREATE TABLE `dados` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `email` varchar(50) NOT NULL,
  `analytics` varchar(100) NOT NULL,
  `endereco` varchar(100) NOT NULL,
  `tel01` varchar(50) NOT NULL,
  `tel02` varchar(50) NOT NULL,
  `tel03` varchar(50) NOT NULL,
  `responsavel` varchar(100) NOT NULL,
  `semana` varchar(50) NOT NULL,
  `fins` varchar(50) NOT NULL,
  `site` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `dados`
--

INSERT INTO `dados` (`id`, `nome`, `email`, `analytics`, `endereco`, `tel01`, `tel02`, `tel03`, `responsavel`, `semana`, `fins`, `site`) VALUES
(1, '', 'ouvidoria@nucleomg.com.br ', '', 'Av. JoÃ£o Pinheiro - 146 - 5Âº e 13Âº andades - Lourdes - Belo Horizonte - MG CEP 30130-922 ', '(31) 3057-9300 ', '(31) 98315-3579 ', '  ', '', '07:00 - 19:00      ', '07:30 - 12:00    ', 'nucleomg.com.br ');

-- --------------------------------------------------------

--
-- Estrutura para tabela `docs_enviados`
--

CREATE TABLE `docs_enviados` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `setor` int(11) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `responsavel` varchar(500) NOT NULL,
  `fim` date NOT NULL,
  `status_admin` int(1) NOT NULL,
  `status_user` int(1) NOT NULL,
  `texto` text NOT NULL,
  `hsh` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `docs_enviados`
--

INSERT INTO `docs_enviados` (`id`, `nome`, `setor`, `slug`, `responsavel`, `fim`, `status_admin`, `status_user`, `texto`, `hsh`) VALUES
(1, 'Exemplo', 1, '', '1', '2018-03-13', 0, 0, '<p>aaa</p>', 'rnt820hnemi'),
(2, 'Exemplo', 1, '', '27', '2018-03-13', 0, 0, '<p>aaa</p>', 'rnt820hnemi'),
(3, '', 0, '', '', '0000-00-00', 0, 0, '', ''),
(4, 'asdf', 4, '', '29', '2018-03-14', 0, 0, '<p>asdf</p>', 'ds9clyfhnw6'),
(5, 'teste', 4, '', '29', '2018-03-14', 0, 0, '<p>teste</p>', 'sfuafto0zdr'),
(6, 'Nome', 4, '', '29', '2018-03-14', 0, 0, '<p>tecxto</p>', '6fu247kfjms'),
(7, 'teste', 4, '', '31', '2018-03-14', 0, 0, '<p>teste</p>', 'skx8fx4rc2e'),
(8, 'teste', 4, '', '30', '2018-03-14', 0, 0, '<p>teste</p>', 'skx8fx4rc2e'),
(9, 'teste', 1, '', '1', '2018-03-15', 0, 0, '<p>dsdf</p>', 'h3tjttlauhd'),
(10, 'Teste de envio de documentaÃ§Ã£o', 21, '', '28', '2018-03-15', 0, 0, '<p>Nome do documento</p>', '3fgvzrc3y3h'),
(11, 'Nome do documento', 21, '', '28', '2018-03-15', 0, 0, '<p>asdf</p>', 'zgcxmppj8ws'),
(12, 'Outro dodumento', 21, '', '28', '2018-03-15', 0, 0, '<p>aaaa</p>', 'b1xxfcwaih3'),
(13, 'Mais um exempl.,o', 21, '', '28', '2018-03-15', 0, 0, '<p>asdfasf</p>', '3869v5khsi2'),
(14, 'Ãšltimo exemplo', 21, '', '28', '2018-03-15', 0, 0, '<p>sdfasdf</p>', 'chbsn5qwqgp'),
(15, 'Nome do documento', 4, '', '29', '2018-03-15', 0, 0, '<p>Texto + obsesrva&ccedil;&otilde;es</p>', 'rvu5qwoocd2'),
(16, 'Nome do documento', 4, '', '31', '2018-03-15', 0, 0, '<p>Texto + obsesrva&ccedil;&otilde;es</p>', 'rvu5qwoocd2'),
(17, 'Nome do documento', 4, '', '30', '2018-03-15', 0, 0, '<p>Texto + obsesrva&ccedil;&otilde;es</p>', 'rvu5qwoocd2'),
(18, 'Outro documento', 4, '', '29', '2018-03-15', 0, 0, '<p>asdf</p>', '4xgwl7bmlc7'),
(19, 'Outro documento', 4, '', '31', '2018-03-15', 0, 0, '<p>asdf</p>', '4xgwl7bmlc7'),
(20, 'Outro documento', 4, '', '30', '2018-03-15', 0, 0, '<p>asdf</p>', '4xgwl7bmlc7'),
(21, 'Teste para mÃ©dicos', 4, '', '29', '2018-03-15', 0, 0, '<p>asdf</p>', 'e7aizf7zu2c'),
(22, 'Teste para mÃ©dicos', 4, '', '31', '2018-03-15', 0, 0, '<p>asdf</p>', 'e7aizf7zu2c'),
(23, 'Teste para mÃ©dicos', 4, '', '30', '2018-03-15', 0, 0, '<p>asdf</p>', 'e7aizf7zu2c'),
(24, 'outro teste', 4, '', '29', '2018-03-15', 0, 0, '<p>asdf</p>', 'glknwh43h5t'),
(25, 'outro teste', 4, '', '31', '2018-03-15', 0, 0, '<p>asdf</p>', 'glknwh43h5t'),
(26, 'outro teste', 4, '', '30', '2018-03-15', 0, 0, '<p>asdf</p>', 'glknwh43h5t'),
(27, 'sfda', 20, '', '28', '2018-03-23', 0, 0, '<p>dgasdf</p>', 'xbv3jke2anz'),
(28, 'sfda', 20, '', '195', '2018-03-23', 0, 0, '<p>dgasdf</p>', 'xbv3jke2anz'),
(29, 'teste', 26, '', '201', '2018-03-23', 0, 0, '<p>Teste</p>', '90kg9r225md'),
(30, 'teste', 26, '', '201', '2018-03-26', 0, 0, '<p>teste</p>', 't78ki3yy8ab'),
(31, 'teste 9', 26, '', '201', '2018-03-26', 0, 0, '<p>teste</p>', 'nyubd0ynl07'),
(32, 'Recibo', 26, '', '201', '2018-03-29', 0, 0, '<p>Segue o recibo referente ao pagamento de mer&ccedil;o de 2018.</p>', 'ffysjsl5ks3'),
(33, 'teste', 1, '', '27', '2018-03-31', 0, 0, '<p>teste</p>', 'tamga2b3qtw');

-- --------------------------------------------------------

--
-- Estrutura para tabela `documentacao_fixa`
--

CREATE TABLE `documentacao_fixa` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `visibilidade` varchar(1000) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `data` date NOT NULL,
  `texto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `documentacao_fixa`
--

INSERT INTO `documentacao_fixa` (`id`, `nome`, `visibilidade`, `slug`, `data`, `texto`) VALUES
(1, 'nome do artigo', 'Coordenador,Supervisor,', 'nome-do-artigo', '2017-09-04', '<p>asdfasd asdf asdfasdf</p>'),
(2, 'outro com Ã£ e Ã§', '', 'outro-com-e-', '2017-09-04', '<p>asdf asdf</p>'),
(3, 'kjukh', '', 'kjukh', '2018-01-19', '<p>&ccedil;jk</p>'),
(4, 'asdf asd f', '', 'asdf-asd-f', '0000-00-00', '<p>asdf as dfasdf</p>'),
(5, 'asdf asd f', '', 'asdf-asd-f', '2018-01-19', '<p>asdf as dfasdf</p>'),
(6, 'fasd', '', 'fasd', '2018-01-19', '<p>asdf</p>'),
(7, 'sf asdfas asd', 'MÃ©dico associado,', 'sf-asdfas-asd', '0000-00-00', '<p>asdf as fasf</p>'),
(8, '', '', '', '0000-00-00', '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `documentos`
--

CREATE TABLE `documentos` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `setor` int(11) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `responsavel` varchar(500) NOT NULL,
  `inicio` date NOT NULL,
  `fim` date NOT NULL,
  `status_admin` int(1) NOT NULL,
  `status_user` int(1) NOT NULL,
  `texto` text NOT NULL,
  `id_pratica` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `documentos`
--

INSERT INTO `documentos` (`id`, `nome`, `setor`, `slug`, `responsavel`, `inicio`, `fim`, `status_admin`, `status_user`, `texto`, `id_pratica`) VALUES
(100, '', 0, '', '28', '2018-04-08', '2018-04-14', 0, 0, '', 47);

-- --------------------------------------------------------

--
-- Estrutura para tabela `doc_associado`
--

CREATE TABLE `doc_associado` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `setor` int(11) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `responsavel` varchar(500) NOT NULL,
  `inicio` date NOT NULL,
  `fim` date NOT NULL,
  `status_admin` int(1) NOT NULL,
  `status_user` int(1) NOT NULL,
  `texto` text NOT NULL,
  `id_pratica` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `doc_associado`
--

INSERT INTO `doc_associado` (`id`, `nome`, `setor`, `slug`, `responsavel`, `inicio`, `fim`, `status_admin`, `status_user`, `texto`, `id_pratica`) VALUES
(107, '', 0, '', '1', '2018-04-09', '2018-04-14', 0, 0, '', 98),
(108, '', 0, '', '27', '2018-04-09', '2018-04-14', 0, 0, '', 98),
(109, '', 0, '', '28', '2018-04-09', '2018-04-14', 0, 0, '', 98);

-- --------------------------------------------------------

--
-- Estrutura para tabela `doc_financeiro`
--

CREATE TABLE `doc_financeiro` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `setor` int(11) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `responsavel` varchar(500) NOT NULL,
  `inicio` date NOT NULL,
  `fim` date NOT NULL,
  `status_admin` int(1) NOT NULL,
  `status_user` int(1) NOT NULL,
  `texto` text NOT NULL,
  `id_pratica` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `doc_financeiro`
--

INSERT INTO `doc_financeiro` (`id`, `nome`, `setor`, `slug`, `responsavel`, `inicio`, `fim`, `status_admin`, `status_user`, `texto`, `id_pratica`) VALUES
(104, '', 0, '', '1', '2018-04-09', '0000-00-00', 0, 0, '', 0),
(105, '', 0, '', '27', '2018-04-09', '0000-00-00', 0, 0, '', 0),
(106, '', 0, '', '28', '2018-04-09', '0000-00-00', 0, 0, '', 1),
(107, '', 0, '', '27', '2018-04-09', '0000-00-00', 0, 0, '', 1),
(109, '', 0, '', '1', '2018-04-09', '0000-00-00', 0, 0, '', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `financeiros`
--

CREATE TABLE `financeiros` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `setor` int(11) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `responsavel` varchar(500) NOT NULL,
  `inicio` date NOT NULL,
  `fim` date NOT NULL,
  `status_admin` int(1) NOT NULL,
  `status_user` int(1) NOT NULL,
  `texto` text NOT NULL,
  `id_pratica` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `financeiros`
--

INSERT INTO `financeiros` (`id`, `nome`, `setor`, `slug`, `responsavel`, `inicio`, `fim`, `status_admin`, `status_user`, `texto`, `id_pratica`) VALUES
(1, 'documento financeiro', 0, '', '', '0000-00-00', '0000-00-00', 0, 0, '<p>asdfasdf asdf</p>', 0),
(2, 'Outro com doc01', 0, '', '', '0000-00-00', '0000-00-00', 0, 0, '<p>asdf</p>', 0),
(3, '', 0, '', '', '0000-00-00', '0000-00-00', 0, 0, '', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `grupos`
--

CREATE TABLE `grupos` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `categoria` int(11) NOT NULL,
  `preco` varchar(10) NOT NULL,
  `cor` varchar(1000) NOT NULL,
  `texto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `grupos`
--

INSERT INTO `grupos` (`id`, `nome`, `slug`, `categoria`, `preco`, `cor`, `texto`) VALUES
(2, 'Vestido 01', 'vestido-01', 1, '12.00', 'amarelo,@azul,branco,cinza,coral,dourado,estampado,laranja,marrom,nude,preto,rosa,roxo,verde,vermelho,vinho', '<p>Informa&ccedil;&otilde;es do produto</p>'),
(3, '', '', 0, '', '', '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `historico`
--

CREATE TABLE `historico` (
  `id` int(11) NOT NULL,
  `resumo` varchar(700) NOT NULL,
  `data` date NOT NULL,
  `tipo` varchar(20) NOT NULL,
  `texto` text NOT NULL,
  `id_processo` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `historico`
--

INSERT INTO `historico` (`id`, `resumo`, `data`, `tipo`, `texto`, `id_processo`, `id_usuario`) VALUES
(18, 'Documento x enviado para y', '2017-05-01', '', 'asdfasdf asdf', 2, 0),
(19, 'outro dado ', '2017-05-02', '', 'asdf', 2, 0),
(20, 'asdfasf', '2017-05-03', '', ' asdfasf asdfaf', 3, 0),
(21, 'hgjhkg', '2017-05-03', '', 'jhgjhg', 2, 0),
(22, 'Comparecer na data da audiencia', '2017-07-19', '', 'Data do dia 26', 8, 0),
(23, 'Aguardando seetenÃ§a', '2017-07-19', '', '', 2, 0),
(24, 'Comparecer na data da audiencia', '2017-07-19', '', '', 10, 0),
(25, 'jhjklhkjh', '2017-07-18', '', '', 10, 0),
(26, 'jhukhkuh', '2017-07-21', '', '', 10, 0),
(27, 'testse', '2017-08-11', '', 'teste', 2, 0),
(28, 'elemento do histÃ³rico', '2017-08-18', '', 'descriÃ§Ã£o do que aconteceu', 4, 0),
(29, 'informaÃ§Ã£o', '2017-09-05', 'servico', 'asdfj asfda asdf lkj asdfj ', 7, 0),
(30, '', '0000-00-00', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `iframes`
--

CREATE TABLE `iframes` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `iframe` varchar(300) NOT NULL,
  `tipo` varchar(100) NOT NULL,
  `vrf` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `imagens`
--

CREATE TABLE `imagens` (
  `id` int(11) NOT NULL,
  `img` varchar(100) NOT NULL,
  `tipo` varchar(100) NOT NULL,
  `verificador` varchar(50) NOT NULL,
  `definitivo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `imagens`
--

INSERT INTO `imagens` (`id`, `img`, `tipo`, `verificador`, `definitivo`) VALUES
(1, 'artigofa8c2137027ed82d8218d433c3b4273e.jpg', 'artigo', '6', 0),
(2, 'artigo97e54e192cb98bc572a5546b1c2b5777.jpg', 'artigo', '6', 0),
(3, 'artigoda8ad154d014ed9403d4c3d5de4bb5c4.pdf', 'artigo', '6', 0),
(4, 'artigo2e892efae2d7fe59d50e26568c89caaf.jpg', 'artigo', '6', 0),
(5, 'artigo64dc8e91b25bbf93cb1388f9ff70c49d.jpg', 'artigo', '6', 0),
(8, 'texto4030d5a1d8985cf9d7fa333f4533a894.jpg', 'texto', '7', 0),
(10, 'textoe6da609f146bde3c73effe8035c6013b.jpg', 'texto', '7', 0),
(11, 'texto11ce196913bf09e50c15beb5fce6a27d.jpg', 'texto', '7', 0),
(12, 'documento4116a3b4d667cadab44d1de5fa5c8706.zip', 'documento', '7', 0),
(13, 'documento1f2d0c5d5e34290d90b2467bfdc2bf07.zip', 'documento', '7', 0),
(14, 'documentofd9fb1be421b731c2e0e0ba6715a9062.zip', 'documento', '7', 0),
(15, 'documento92e0e0e0823738a5b1db9d9be056ea0a.zip', 'documento', '7', 0),
(16, 'perfil1da0f4a0ed2598af50dff82f51ceccdd.png', 'perfil', '6', 0),
(18, 'atividadeb0480ba207a898678054fea22b1c1e73.png', 'atividade', '1', 0),
(19, 'atividade9519c1c38ecea352dbc37d6988fc59d3.pdf', 'atividade', '1', 0),
(20, 'atividade4a95423714c19e4103b7c876a6df92b2.pdf', 'atividade', '2', 0),
(21, 'atividadec1090e0c089756be5d9a5e7847d262e6.jpg', 'atividade', '2', 0),
(24, 'doc_setor5638e485726005944e9de7b190601a60.png', 'doc_setor', '4', 0),
(25, 'doc_setor1c4ccc89834ce1bb6a78c2df2d3a1482.pdf', 'doc_setor', '4', 0),
(26, 'atividade07e290426ebca410b125540dc58f66f0.pdf', 'atividade', '14', 0),
(27, 'atividadeff346395f2c9684845f7830a143bfe04.jpg', 'atividade', '14', 0),
(31, 'atividadee1be672fdcc8fb70e9223214c240369d.pdf', 'atividade', '17', 0),
(32, 'atividade9bba4967cb8883367c6d757d65538499.png', 'atividade', '17', 0),
(34, 'atividade39c12af394b688d8582a8078c2ff0670.png', 'atividade', '18', 0),
(35, 'atividade88e3c7cca43bb297c2f9f660feaa42c5.png', 'atividade', '18', 0),
(36, 'atividadef6d4a91535f67214b84d7c3638836282.png', 'atividade', '18', 0),
(37, 'atividadeeda29397a87217f5cf57f80cd03ae9a9.jpg', 'atividade', '18', 0),
(38, 'atividadefb5c91cb4ec7096371e002ca58cb5bb9.png', 'atividade', '19', 0),
(39, 'atividade11c1e8278837e27db435b9db4e50776e.png', 'atividade', '19', 0),
(40, 'atividade3b3cf4b653a64daeba2e36cc42505c75.png', 'atividade', '19', 0),
(43, 'perfil474c95ec24b06b623080549408142857.jpg', 'perfil', '33', 0),
(44, 'perfil9de8e1bea850035798b0bfd62f79c815.png', 'perfil', '31', 0),
(45, 'perfil788102b4db18b4f4ba0add8a5b5f9e2d.jpg', 'perfil', '37', 0),
(46, 'doc_enviado95ef0966b2bdf20793fabb20d629fe54.png', 'doc_enviado', 'HyZiVcrTFyy', 0),
(47, 'doc_enviado8d1bacb56840eca15ef9c7be0c55bdab.png', 'doc_enviado', 'HyZiVcrTFyy', 0),
(48, 'doc_enviadoa8ace673a6828da177a353084421dd5b.png', 'doc_enviado', 'HyZiVcrTFyy', 0),
(49, 'doc_enviado4a1951718debf5fd307df9be0834048e.png', 'doc_enviado', 'OOiqruoR7Vs', 0),
(50, 'doc_enviadofe1856bcf427da58a16a768ce055bcc4.png', 'doc_enviado', 'OOiqruoR7Vs', 0),
(51, 'doc_enviadod2b097f04059e8c1c15034679ce8f2ce.png', 'doc_enviado', 'OOiqruoR7Vs', 0),
(52, 'doc_enviado544225964d14f032375bf5e489026c53.png', 'doc_enviado', '9u4zjxfx7or', 0),
(53, 'doc_enviado91463abb212431ac5585d652c28d3345.png', 'doc_enviado', 'usau15d09du', 0),
(54, 'doc_enviadof9b965f2f71e630e039c9fc182c9d8a8.png', 'doc_enviado', '0u18zcakp78', 0),
(55, 'doc_enviado5c29ca887b877ecdb1b91426aa780b24.png', 'doc_enviado', 'tlawf99hc1g', 0),
(56, 'doc_enviado943ff14af967d1beb7cb2e7b22f2f156.png', 'doc_enviado', 'tlawf99hc1g', 0),
(57, 'doc_enviadofb499cd389497c08973a7486ab62384a.png', 'doc_enviado', 'tlawf99hc1g', 0),
(58, 'doc_enviadof1780bcdfbdac77c6d6aa00e2b0b36f5.png', 'doc_enviado', 'ei5hapbbhl9', 0),
(59, 'doc_enviado4ed8b2b23142803f0f9a9eb59579d350.png', 'doc_enviado', 'rnt820hnemi', 0),
(60, 'perfil63fbded038dd5fb9f5b38635ec0ff17d.png', 'perfil', '28', 0),
(61, 'doc_enviado3fa07a728bd2f421927f792b0e76c884.pdf', 'doc_enviado', 'umi92smx911', 0),
(62, 'doc_enviadoef0ef35534ddcf1b99a38cc4a2b1e7f7.png', 'doc_enviado', 'fp5r08n3n9l', 0),
(63, 'doc_enviado233edc0f15f735c5cd5a8edccd6acc9f.png', 'doc_enviado', 'ds9clyfhnw6', 0),
(64, 'doc_enviado029241bfdc3b4525dad019dc6d5dfede.jpg', 'doc_enviado', 'eu0eoskhgic', 0),
(65, 'doc_enviadoea20e1785896ce891f2c8da1c51c1f73.pdf', 'doc_enviado', 'sacr01jy7b1', 0),
(66, 'doc_enviadod5a25831bd27edadda73a1be34b7ed11.pdf', 'doc_enviado', 'sfuafto0zdr', 0),
(67, 'doc_enviadod1e73bf5df6c8369bbd7872b31c5289e.pdf', 'doc_enviado', '6fu247kfjms', 0),
(68, 'doc_enviado978562d5d5e3a60c81e7a5cbd87eec05.jpg', 'doc_enviado', 'skx8fx4rc2e', 0),
(69, 'doc_enviadobd6e76cd489cc7f786cfcc3f0d898acb.png', 'doc_enviado', 'dj4of3zp48u', 0),
(70, 'doc_enviadoba8385bddc35b2e0ee3acccd127e7cde.png', 'doc_enviado', 'h3tjttlauhd', 0),
(71, 'doc_enviadod8674c7f3c3c32ad09c88c4e91fe70af.png', 'doc_enviado', '2pcvpzqqb0z', 0),
(72, 'doc_enviado3c6f68cd2500f05909704f0f0449fd86.png', 'doc_enviado', '3fgvzrc3y3h', 0),
(73, 'doc_enviado2c04789d9f67ba7632056a2568164ab1.png', 'doc_enviado', 'zgcxmppj8ws', 0),
(74, 'doc_enviadob814fbc3dbd65a977d3508fd4dfc6b97.png', 'doc_enviado', 'b1xxfcwaih3', 0),
(75, 'doc_enviadof719f2850dcb472adf3def893f53c318.png', 'doc_enviado', '3869v5khsi2', 0),
(76, 'doc_enviadoce956c07587ed77d6ab638b78d51d11f.png', 'doc_enviado', 'chbsn5qwqgp', 0),
(77, 'doc_enviado85fadbf4e2da7fb308ef7f06faed3964.png', 'doc_enviado', 'rvu5qwoocd2', 0),
(78, 'doc_enviado02261191dbb6b37749a02945520bcf62.png', 'doc_enviado', '4xgwl7bmlc7', 0),
(79, 'doc_enviado049d1d965c533cbec7c567584a9e4ace.png', 'doc_enviado', 'e7aizf7zu2c', 0),
(80, 'doc_enviadoe021a4f6978f05b230143c854d055fb3.png', 'doc_enviado', '8bd87c6i3u3', 0),
(81, 'doc_enviado0c72231ab8413174cad95260539842e6.png', 'doc_enviado', 'glknwh43h5t', 0),
(82, 'slidef55aca7a9069ebe142de3fd900300dee.jpg', 'slide', '9', 0),
(83, 'doc_enviado45f1876859e75a08d720f095ae88771d.pdf', 'doc_enviado', 'xbv3jke2anz', 0),
(84, 'doc_enviado2cbb06b6d10dec06080e067e792ef05b.jpg', 'doc_enviado', 'l7b9s03m536', 0),
(85, 'doc_enviado59137ec1c2926b204eb8dd08ad16b851.jpg', 'doc_enviado', '90kg9r225md', 0),
(86, 'doc_enviadoe62d4647a3c11c81d227ac476c4ee3e0.jpg', 'doc_enviado', 't78ki3yy8ab', 0),
(87, 'doc_enviadoefd32d8792160dcf0f3c48d832270ffb.jpg', 'doc_enviado', 'lkhcdlc2uao', 0),
(88, 'doc_enviado2e7bff03e5969f6a0a6f1df806601c37.jpg', 'doc_enviado', 'nyubd0ynl07', 0),
(90, 'beneficio8b4d002d4af06609bd7c2b8db947ecf4.png', 'beneficio', '7', 0),
(91, 'doc_enviadob46c4c2d5de890b57b3a5154c0a5d816.png', 'doc_enviado', 'ffysjsl5ks3', 0),
(92, 'perfil54255261d28008e80c75af32bc096c4f.jpg', 'perfil', '199', 0),
(93, 'doc_enviado8e1533dd52d48cf7ce264872b4eb0c74.jpg', 'doc_enviado', 'tamga2b3qtw', 0),
(94, 'perfild71a86432300715558e0c6f7053b76e3.jpg', 'perfil', '27', 0),
(95, 'atividade7fd52a592c036dbf33225b8f9279e4bb.pdf', 'atividade', '47', 0),
(96, 'atividade130c73ce25347d6ee2fab4124edac555.jpg', 'atividade', '47', 0),
(98, 'doc0234c815484dd2a6b980e7b989de750458.png', 'doc02', '2', 0),
(99, 'doc0282e0ac4aff99a0449305f8f1cac7b0bb.jpg', 'doc02', '1', 0),
(104, 'doc012ceb248d74fbe8babc90ac974b289203.jpg', 'doc01', '100', 0),
(105, 'doc016428bb0e563fc96dcbcc79ccf77aa0f8.png', 'doc01', '100', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `item`
--

CREATE TABLE `item` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `fornecedor` int(10) NOT NULL,
  `qte` int(11) NOT NULL,
  `data_compra` date NOT NULL,
  `data_consumo` date NOT NULL,
  `preco` varchar(10) NOT NULL,
  `tipo` varchar(20) NOT NULL,
  `situacao` varchar(20) NOT NULL,
  `texto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `item`
--

INSERT INTO `item` (`id`, `nome`, `fornecedor`, `qte`, `data_compra`, `data_consumo`, `preco`, `tipo`, `situacao`, `texto`) VALUES
(17, '', 0, 0, '0000-00-00', '0000-00-00', '', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `lista`
--

CREATE TABLE `lista` (
  `id` int(11) NOT NULL,
  `id_locacao` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `qte` int(11) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `lista`
--

INSERT INTO `lista` (`id`, `id_locacao`, `id_produto`, `qte`, `status`) VALUES
(5, 2, 22, 1, 'ok'),
(9, 3, 23, 3, 'ok'),
(10, 3, 22, 9, 'ok');

-- --------------------------------------------------------

--
-- Estrutura para tabela `locacao`
--

CREATE TABLE `locacao` (
  `id` int(11) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `endereco` varchar(500) NOT NULL,
  `inicio` date NOT NULL,
  `fim` date NOT NULL,
  `dias` int(11) NOT NULL,
  `telefones` varchar(500) NOT NULL,
  `status` varchar(20) NOT NULL,
  `situacao` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `locacao`
--

INSERT INTO `locacao` (`id`, `nome`, `endereco`, `inicio`, `fim`, `dias`, `telefones`, `status`, `situacao`) VALUES
(1, 'fulano', 'adsf', '2017-12-19', '2017-12-21', 2, '1|2|3', 'ok', ''),
(2, 'asf', 'endereÃ§o', '2017-12-21', '2017-12-22', 3, '1|2|3', 'ok', 'retirado'),
(3, 'outro', 'endereÃ§o', '2017-12-19', '2017-12-26', 7, '1|1|1', 'ok', ''),
(4, 'asdf', 'atletico mineiro, 8', '2017-12-12', '2017-12-20', 8, 'adf|a|a', 'marcado', ''),
(5, 'asdf ', '', '0000-00-00', '0000-00-00', 0, '', 'marcado', '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `options`
--

CREATE TABLE `options` (
  `id` int(11) NOT NULL,
  `option_name` varchar(30) NOT NULL,
  `option_value` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `paginas`
--

CREATE TABLE `paginas` (
  `id` int(11) NOT NULL,
  `nome` varchar(1000) NOT NULL,
  `texto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `paginas`
--

INSERT INTO `paginas` (`id`, `nome`, `texto`) VALUES
(1, 'Regulamentos', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nibh lacus, aliquam sed dignissim nec, commodo ac ipsum. Sed dapibus efficitur est, nec finibus mi ullamcorper viverra. Aliquam non placerat tortor. Donec pellentesque a urna id viverra. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nam vulputate arcu eget eros iaculis sodales. In finibus faucibus finibus. Cras varius rutrum ipsum, eu ultrices ligula lacinia eget. Morbi pulvinar ex sed volutpat bibendum. Nulla facilisi. Nulla bibendum odio eu enim euismod vestibulum.</p>\r\n<p>Donec ac ante sapien. In vulputate eros et nulla commodo, eget finibus nunc commodo. Praesent tincidunt vitae tortor et maximus. Vivamus fermentum massa ligula, a facilisis sapien bibendum eu. Nam ante justo, euismod sed tellus non, rhoncus suscipit ligula. Phasellus sit amet urna eu nibh ultricies blandit. Donec fermentum nisl ut dui semper, sed ornare metus ullamcorper. Interdum et malesuada fames ac ante ipsum primis in faucibus. Cras mollis sagittis nisi eget tincidunt. Aenean eget massa felis. Suspendisse quis suscipit ex, sed finibus mi. Curabitur feugiat dui in eros vulputate, et ultricies tortor lacinia. Mauris diam velit, consectetur eget molestie vitae, pharetra vel nisl. Phasellus consectetur porttitor mauris at accumsan.</p>\r\n<p>Donec vulputate mi id accumsan semper. Donec porttitor dapibus tellus, id vulputate elit consequat mollis. Aliquam gravida nec eros vel dapibus. Donec et euismod enim, at pulvinar felis. Aenean a sem auctor, rutrum metus at, eleifend turpis. Sed convallis, quam at luctus sodales, ligula arcu rhoncus quam, ut condimentum dui enim non magna. Pellentesque eu dolor sem. Sed congue suscipit nibh, sit amet accumsan purus. Maecenas vitae pellentesque tellus, et hendrerit nisl.</p>'),
(2, 'Perguntas frequente', '<p><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</strong></p>\r\n<p>Quisque nibh lacus, aliquam sed dignissim nec, commodo ac ipsum. Sed dapibus efficitur est, nec finibus mi ullamcorper viverra. Aliquam non placerat tortor. Donec pellentesque a urna id viverra. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</strong></p>\r\n<p>Quisque nibh lacus, aliquam sed dignissim nec, commodo ac ipsum. Sed dapibus efficitur est, nec finibus mi ullamcorper viverra. Aliquam non placerat tortor. Donec pellentesque a urna id viverra. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</strong></p>\r\n<p>Quisque nibh lacus, aliquam sed dignissim nec, commodo ac ipsum. Sed dapibus efficitur est, nec finibus mi ullamcorper viverra. Aliquam non placerat tortor. Donec pellentesque a urna id viverra. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</strong></p>\r\n<p>Quisque nibh lacus, aliquam sed dignissim nec, commodo ac ipsum. Sed dapibus efficitur est, nec finibus mi ullamcorper viverra. Aliquam non placerat tortor. Donec pellentesque a urna id viverra. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>\r\n<p>&nbsp;</p>'),
(3, 'Entregas e devoluções', ''),
(4, 'Política de privacidade', ''),
(5, 'Contrato de aluguel', ''),
(6, 'Quem somos', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nibh lacus, aliquam sed dignissim nec, commodo ac ipsum. Sed dapibus efficitur est, nec finibus mi ullamcorper viverra. Aliquam non placerat tortor. Donec pellentesque a urna id viverra. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nam vulputate arcu eget eros iaculis sodales. In finibus faucibus finibus. Cras varius rutrum ipsum, eu ultrices ligula lacinia eget. Morbi pulvinar ex sed volutpat bibendum. Nulla facilisi. Nulla bibendum odio eu enim euismod vestibulum.</p>\r\n<p>Donec ac ante sapien. In vulputate eros et nulla commodo, eget finibus nunc commodo. Praesent tincidunt vitae tortor et maximus. Vivamus fermentum massa ligula, a facilisis sapien bibendum eu. Nam ante justo, euismod sed tellus non, rhoncus suscipit ligula. Phasellus sit amet urna eu nibh ultricies blandit. Donec fermentum nisl ut dui semper, sed ornare metus ullamcorper. Interdum et malesuada fames ac ante ipsum primis in faucibus. Cras mollis sagittis nisi eget tincidunt. Aenean eget massa felis. Suspendisse quis suscipit ex, sed finibus mi. Curabitur feugiat dui in eros vulputate, et ultricies tortor lacinia. Mauris diam velit, consectetur eget molestie vitae, pharetra vel nisl. Phasellus consectetur porttitor mauris at accumsan.</p>\r\n<p>Donec vulputate mi id accumsan semper. Donec porttitor dapibus tellus, id vulputate elit consequat mollis. Aliquam gravida nec eros vel dapibus. Donec et euismod enim, at pulvinar felis. Aenean a sem auctor, rutrum metus at, eleifend turpis. Sed convallis, quam at luctus sodales, ligula arcu rhoncus quam, ut condimentum dui enim non magna. Pellentesque eu dolor sem. Sed congue suscipit nibh, sit amet accumsan purus. Maecenas vitae pellentesque tellus, et hendrerit nisl.</p>');

-- --------------------------------------------------------

--
-- Estrutura para tabela `papelaria`
--

CREATE TABLE `papelaria` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `papelaria`
--

INSERT INTO `papelaria` (`id`, `nome`) VALUES
(1, 'nome'),
(2, 'sdfasdf'),
(3, 'wadsf'),
(4, 'wadsf'),
(5, 'sadf'),
(6, 'asdfgsdf'),
(7, 'exemplo01'),
(8, '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `pergunta`
--

CREATE TABLE `pergunta` (
  `id` int(11) NOT NULL,
  `id_pergunta` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `tipo` varchar(20) NOT NULL,
  `texto` text NOT NULL,
  `resposta` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `pergunta`
--

INSERT INTO `pergunta` (`id`, `id_pergunta`, `id_usuario`, `tipo`, `texto`, `resposta`) VALUES
(22, 17, 27, 'pergunta', 'Texto da pergunta', 'resposta 000'),
(23, 19, 32, 'pergunta', 'Mensagem de dÃºvida', 'Resposta para o usuÃ¡rio'),
(24, 16, 27, 'pergunta', 'Teste de envio 12:19h', 'resposta'),
(25, 16, 27, 'pergunta', 'Tive problemas', 'Ganhou um dia prazo.'),
(26, 41, 201, 'pergunta', 'Envio concluÃ­do.', 'O envio nÃ£o foi concluÃ­do, gentileza verificar o que aconteceu na entrega. Ela ainda estÃ¡ dentro do prazo.'),
(27, 70, 28, 'pergunta', 'mensagem', 'Resposta'),
(28, 79, 28, 'pergunta', 'PErgunta x', 'resposta x');

-- --------------------------------------------------------

--
-- Estrutura para tabela `praticas`
--

CREATE TABLE `praticas` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `setor` int(11) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `responsavel` varchar(500) NOT NULL,
  `inicio` date NOT NULL,
  `fim` date NOT NULL,
  `status` int(1) NOT NULL,
  `texto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `praticas`
--

INSERT INTO `praticas` (`id`, `nome`, `setor`, `slug`, `responsavel`, `inicio`, `fim`, `status`, `texto`) VALUES
(47, 'Atividade de exemplo', 0, '', '', '2018-04-08', '2018-04-14', 0, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin nec ipsum rutrum, porttitor lorem ac, viverra nunc. Phasellus consequat non nisl et ullamcorper. Aenean luctus ex non dolor porttitor congue. Vivamus sed maximus eros. Aenean cursus sit amet quam et interdum. Vestibulum lectus elit, volutpat eu scelerisque a, lacinia a ex.</p>'),
(48, 'teste', 0, '', '', '2018-04-09', '2018-04-09', 0, '<p>teste</p>'),
(49, '', 0, '', '', '0000-00-00', '0000-00-00', 0, '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `processos`
--

CREATE TABLE `processos` (
  `id` int(11) NOT NULL,
  `id_usuario` varchar(150) NOT NULL,
  `numero` varchar(100) NOT NULL,
  `data_registro` date NOT NULL,
  `texto` text NOT NULL,
  `parte_adversa` varchar(100) NOT NULL,
  `audiencia` date NOT NULL,
  `situacao` varchar(100) NOT NULL,
  `comarca` varchar(100) NOT NULL,
  `tipo` varchar(100) NOT NULL,
  `valorfinal` varchar(100) NOT NULL,
  `resultado` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `produtos`
--

CREATE TABLE `produtos` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `grupo` int(11) NOT NULL,
  `qte` int(11) NOT NULL,
  `preco` varchar(10) NOT NULL,
  `texto` text NOT NULL,
  `categoria` varchar(11) NOT NULL,
  `cor` varchar(500) NOT NULL,
  `tamanhos` varchar(500) NOT NULL,
  `tipo` varchar(50) NOT NULL,
  `slug` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `produtos`
--

INSERT INTO `produtos` (`id`, `nome`, `grupo`, `qte`, `preco`, `texto`, `categoria`, `cor`, `tamanhos`, `tipo`, `slug`) VALUES
(19, 'x', 2, 10, '', '', '', '', '35', '', ''),
(22, 'nome do rpduto', 0, 10, '111.11', '<p>asdf</p>', '3', '', '', 'produto', 'nome-do-rpduto'),
(23, 'sdfgdfg', 0, 3, '2', '<p>sdfasdf asfd</p>', '3', '', '', 'produto', 'sdfgdfg'),
(24, '', 0, 0, '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `ramais`
--

CREATE TABLE `ramais` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `setor` varchar(150) NOT NULL,
  `numero` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `ramais`
--

INSERT INTO `ramais` (`id`, `nome`, `setor`, `numero`) VALUES
(2, 'Wellington - Gerente de TI', 'TI', '305793013'),
(3, 'Adelson - Gerente Adminsitrativo e Financeiro', 'Gerencia', '3057 9336'),
(4, '', '', '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `servicos`
--

CREATE TABLE `servicos` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `preco` varchar(10) NOT NULL,
  `tipo` varchar(20) NOT NULL,
  `situacao` varchar(20) NOT NULL,
  `texto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `servicos`
--

INSERT INTO `servicos` (`id`, `nome`, `preco`, `tipo`, `situacao`, `texto`) VALUES
(17, 'ServiÃ§o 2', '321,23', '', '', '<p>teste</p>'),
(20, 'Consulta JurÃ­dica', '150,00', '', '', '<p>Consulta Simples</p>'),
(21, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `sessao`
--

CREATE TABLE `sessao` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `sessao`
--

INSERT INTO `sessao` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('9gihtmi7qrr4jrdbm63mune2lcv3pt10', '::1', 1516227049, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531363232373034393b757365724c6f6761646f7c4f3a383a22737464436c617373223a32383a7b733a323a226964223b733a313a2236223b733a373a227573756172696f223b733a31353a22656d61696c40656d61696c2e636f6d223b733a353a2273656e6861223b733a353a2273656e6861223b733a343a227469706f223b733a343a2275736572223b733a363a22737461747573223b733a383a22726563757361646f223b733a373a2269645f75736572223b733a313a2230223b733a343a226e6f6d65223b733a353a226f7574726f223b733a363a226d6f7469766f223b733a32373a22436f6e747261746f7320202f205461786173206162757369766173223b733a373a22746578746f3031223b733a343a2261736466223b733a373a22746578746f3032223b733a343a2277657274223b733a363a22667261756465223b733a333a2273696d223b733a393a22636f6e73756c746f75223b733a363a224ec383c2a36f223b733a31373a22656d70726573615f7265636c616d616461223b733a323a226768223b733a343a22636e706a223b733a333a22657472223b733a383a22656e64657265636f223b733a333a22657274223b733a353a2272617a616f223b733a343a2277657274223b733a373a226175746f726972223b733a303a22223b733a31333a226e6f6d655f636f6d706c65746f223b733a31343a226173646661736466206173646661223b733a31323a2265737461646f5f636976696c223b733a303a22223b733a333a22637066223b733a303a22223b733a323a227267223b733a303a22223b733a393a2270726f66697373616f223b733a303a22223b733a353a2272656e6461223b733a303a22223b733a363a22636f6e746172223b733a303a22223b733a393a227265736f6c7563616f223b733a303a22223b733a383a22636f6e636f726461223b733a303a22223b733a393a2274656c65666f6e6573223b733a303a22223b733a31353a226e756d65726f5f70726f636573736f223b733a303a22223b7d6c6f6761646f7c623a313b),
('bi372bo13reuvbdc7n9ll314hnb3c5vo', '::1', 1516226482, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531363232363438323b757365724c6f6761646f7c4f3a383a22737464436c617373223a32383a7b733a323a226964223b733a313a2236223b733a373a227573756172696f223b733a31353a22656d61696c40656d61696c2e636f6d223b733a353a2273656e6861223b733a353a2273656e6861223b733a343a227469706f223b733a343a2275736572223b733a363a22737461747573223b733a383a22726563757361646f223b733a373a2269645f75736572223b733a313a2230223b733a343a226e6f6d65223b733a353a226f7574726f223b733a363a226d6f7469766f223b733a32373a22436f6e747261746f7320202f205461786173206162757369766173223b733a373a22746578746f3031223b733a343a2261736466223b733a373a22746578746f3032223b733a343a2277657274223b733a363a22667261756465223b733a333a2273696d223b733a393a22636f6e73756c746f75223b733a363a224ec383c2a36f223b733a31373a22656d70726573615f7265636c616d616461223b733a323a226768223b733a343a22636e706a223b733a333a22657472223b733a383a22656e64657265636f223b733a333a22657274223b733a353a2272617a616f223b733a343a2277657274223b733a373a226175746f726972223b733a303a22223b733a31333a226e6f6d655f636f6d706c65746f223b733a31343a226173646661736466206173646661223b733a31323a2265737461646f5f636976696c223b733a303a22223b733a333a22637066223b733a303a22223b733a323a227267223b733a303a22223b733a393a2270726f66697373616f223b733a303a22223b733a353a2272656e6461223b733a303a22223b733a363a22636f6e746172223b733a303a22223b733a393a227265736f6c7563616f223b733a303a22223b733a383a22636f6e636f726461223b733a303a22223b733a393a2274656c65666f6e6573223b733a303a22223b733a31353a226e756d65726f5f70726f636573736f223b733a303a22223b7d6c6f6761646f7c623a313b),
('d5tvkbu5ej50m6bfuspahp51kfhhnpon', '::1', 1516224221, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531363232343232313b),
('kdihsitt33jokj6c3dvc3d09h198s4i4', '::1', 1516227183, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531363232373034393b757365724c6f6761646f7c4f3a383a22737464436c617373223a32383a7b733a323a226964223b733a313a2236223b733a373a227573756172696f223b733a31353a22656d61696c40656d61696c2e636f6d223b733a353a2273656e6861223b733a353a2273656e6861223b733a343a227469706f223b733a343a2275736572223b733a363a22737461747573223b733a383a22726563757361646f223b733a373a2269645f75736572223b733a313a2230223b733a343a226e6f6d65223b733a353a226f7574726f223b733a363a226d6f7469766f223b733a32373a22436f6e747261746f7320202f205461786173206162757369766173223b733a373a22746578746f3031223b733a343a2261736466223b733a373a22746578746f3032223b733a343a2277657274223b733a363a22667261756465223b733a333a2273696d223b733a393a22636f6e73756c746f75223b733a363a224ec383c2a36f223b733a31373a22656d70726573615f7265636c616d616461223b733a323a226768223b733a343a22636e706a223b733a333a22657472223b733a383a22656e64657265636f223b733a333a22657274223b733a353a2272617a616f223b733a343a2277657274223b733a373a226175746f726972223b733a303a22223b733a31333a226e6f6d655f636f6d706c65746f223b733a31343a226173646661736466206173646661223b733a31323a2265737461646f5f636976696c223b733a303a22223b733a333a22637066223b733a303a22223b733a323a227267223b733a303a22223b733a393a2270726f66697373616f223b733a303a22223b733a353a2272656e6461223b733a303a22223b733a363a22636f6e746172223b733a303a22223b733a393a227265736f6c7563616f223b733a303a22223b733a383a22636f6e636f726461223b733a303a22223b733a393a2274656c65666f6e6573223b733a303a22223b733a31353a226e756d65726f5f70726f636573736f223b733a303a22223b7d6c6f6761646f7c623a313b),
('ombi1prg0kj89uefgsovr87fqvk2u8d4', '::1', 1516224646, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531363232343634363b);

-- --------------------------------------------------------

--
-- Estrutura para tabela `setores`
--

CREATE TABLE `setores` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `texto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `setores`
--

INSERT INTO `setores` (`id`, `nome`, `texto`) VALUES
(1, 'Administrativo', ''),
(2, 'Financeiro', '<p>dfasdf sadf</p>'),
(4, 'MÃ©dicos SÃ³cios', '<p>Alguma informa&ccedil;&atilde;o adicional</p>'),
(8, 'Almoxarifado', ''),
(9, 'Centro CirÃºrgico', ''),
(10, 'Comercial', ''),
(11, 'DigitaÃ§Ã£o', ''),
(12, 'Enfermagem', ''),
(13, 'FarmÃ¡cia', ''),
(14, 'Faturamento', ''),
(15, 'HigienizaÃ§Ã£o', ''),
(16, 'MarcaÃ§Ã£o', ''),
(17, 'MÃ©dicos NÃ£o-SÃ³cios', ''),
(18, 'Qualidade', ''),
(19, 'RecepÃ§Ã£o', ''),
(20, 'RH', ''),
(21, 'Tesouraria', ''),
(22, 'TI', ''),
(23, 'Coordenadores', ''),
(24, 'Supervisores', ''),
(25, 'Colaboradores', ''),
(26, 'GRUPO DE TESTE', ''),
(27, '', '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `slides`
--

CREATE TABLE `slides` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `link` varchar(150) NOT NULL,
  `fim` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `slides`
--

INSERT INTO `slides` (`id`, `nome`, `link`, `fim`) VALUES
(9, 'Banner institucional', 'http://nucleomg.com.br/', '2018-12-31'),
(10, '', '', '0000-00-00');

-- --------------------------------------------------------

--
-- Estrutura para tabela `solicitacoes`
--

CREATE TABLE `solicitacoes` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `data_retirada` date NOT NULL,
  `data_devolucao` date NOT NULL,
  `id_produto` int(10) NOT NULL,
  `situacao` varchar(20) NOT NULL,
  `texto` text NOT NULL,
  `id_cliente` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `solicitacoes`
--

INSERT INTO `solicitacoes` (`id`, `nome`, `data_retirada`, `data_devolucao`, `id_produto`, `situacao`, `texto`, `id_cliente`) VALUES
(1, 'Nome do servico 01', '2017-08-21', '2017-09-01', 1, 'terminou', 'asdf asdf a asdfasdf', 1),
(2, 'Nome do servico 01', '2017-08-21', '2017-09-01', 1, 'solicitado', 'asdf asdf a asdfasdf', 2),
(3, 'sdf', '2017-08-30', '0000-00-00', 3, 'solicitado', 'asdf asdfasfd asdf ', 0),
(4, 'Nome do servico 03', '2017-08-21', '2017-09-01', 1, 'andamento', 'asdf asdf a asdfasdf', 1),
(5, 'Nome do servico 04', '2017-08-21', '2017-09-01', 1, 'terminou', 'asdf asdf a asdfasdf', 2),
(6, 'Nome do servico 01', '2017-08-21', '2017-09-01', 1, 'andamento', 'asdf asdf a asdfasdf', 1),
(7, 'Nome do servico 01', '2017-08-21', '2017-09-01', 1, 'andamento', 'asdf asdf a asdfasdf', 2);

-- --------------------------------------------------------

--
-- Estrutura para tabela `textos`
--

CREATE TABLE `textos` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `texto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `textos`
--

INSERT INTO `textos` (`id`, `nome`, `slug`, `texto`) VALUES
(1, 'nome do artigo 01', 'nome-do-artigo-01', '<p>asdfasd asdf asdfasdf</p>'),
(2, 'outro com Ã£ e Ã§ 01', 'outro-com-a-e-c-01', '<p>asdf asdf</p>'),
(3, 'cliente', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla dapibus vitae tortor non ultrices. Phasellus non luctus nunc. Sed lacinia quam lacus, a feugiat velit congue sed. Aenean hendrerit enim sed dolor faucibus tempor. Ut pulvinar nisi id ex ornare tempor. Nulla scelerisque augue in tortor tristique ullamcorper in vitae erat. Duis aliquet magna at nisl aliquam maximus. In egestas dui vitae rutrum elementum. Suspendisse potenti. Integer pellentesque rutrum erat, a pharetra risus eleifend sed.</p>\r\n<p>Phasellus pellentesque, purus eget dignissim imperdiet, urna urna iaculis diam, a feugiat purus lectus eu ipsum. Suspendisse ac ipsum molestie, ullamcorper metus non, iaculis diam. Nam et porttitor felis. Integer tincidunt sapien purus, ac facilisis urna scelerisque ac. Sed sed urna felis. Nam blandit tristique mauris. Suspendisse a felis nec sapien volutpat gravida vel vel elit. Curabitur consequat imperdiet neque id lacinia. In malesuada sagittis nisl, vel scelerisque lacus tempus quis. Mauris quis lorem id ante ornare scelerisque. In nec porta nulla, vel scelerisque ligula. Donec at rhoncus nibh. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\r\n<p>Nullam in suscipit ante. Proin a turpis commodo, dictum diam at, pretium mi. Morbi vel tristique lorem, at fringilla elit. Maecenas ultrices eu nisi a tincidunt. Morbi lectus odio, scelerisque ut nibh vel, iaculis suscipit turpis. Donec vel diam non eros accumsan efficitur ac vitae lorem. Curabitur euismod nulla ipsum, vel lobortis sapien congue vitae. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla facilisi. Nam viverra, odio vitae rutrum ornare, dolor metus suscipit metus, non semper tortor mauris eget ligula. Quisque at euismod metus. Maecenas est sem, suscipit ut vulputate non, eleifend non tortor.</p>'),
(5, '', '', '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tipo_de_usuario`
--

CREATE TABLE `tipo_de_usuario` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `tipo_de_usuario`
--

INSERT INTO `tipo_de_usuario` (`id`, `nome`) VALUES
(1, 'Administrador'),
(2, 'Médico associado'),
(3, 'Financeiro'),
(4, 'Coordenador'),
(7, 'Padrão');

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuarioz`
--

CREATE TABLE `usuarioz` (
  `id` int(11) NOT NULL,
  `usuario` varchar(120) NOT NULL,
  `email` varchar(100) NOT NULL,
  `senha` varchar(20) NOT NULL,
  `tipo` varchar(15) NOT NULL,
  `status` varchar(15) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `exibir` int(1) NOT NULL,
  `dia` int(11) NOT NULL,
  `mes` char(20) NOT NULL,
  `ano` int(11) NOT NULL,
  `data` date NOT NULL,
  `texto` text NOT NULL,
  `setor01` int(3) NOT NULL,
  `setor02` int(3) NOT NULL,
  `setor03` int(3) NOT NULL,
  `grupos` varchar(1500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `usuarioz`
--

INSERT INTO `usuarioz` (`id`, `usuario`, `email`, `senha`, `tipo`, `status`, `nome`, `exibir`, `dia`, `mes`, `ano`, `data`, `texto`, `setor01`, `setor02`, `setor03`, `grupos`) VALUES
(1, '', 'Wellington Vieira', 'nucleo@123', '1', 'ok', 'Wellington Vieira', 0, 1, 'Fevereiro', 0, '0000-00-00', '', 1, 2, 0, '(1)'),
(27, 'user', 'email@email.com.br', '0102', '2', '', 'UsuÃ¡rio teste', 0, 2, 'Janeiro', 0, '0000-00-00', '<p>Teste</p>', 1, 26, 0, '(1)'),
(28, 'gutemberg', 'contato@agencia7.com.br', '0102', '7', '', 'Gutemberg Costa', 0, 3, 'Dezembro', 0, '0000-00-00', '', 21, 20, 26, '(26)(1)'),
(31, '', 'compras@nucleomg.com.br', '1234', '2', '', 'JoÃ£o Cordeiro', 0, 4, 'Novembro', 0, '0000-00-00', '', 4, 0, 0, ''),
(32, 'Alaor JÃºnior', 'design.brunosantos@gmail.com', '1234', '2', '', 'Alaor JÃºnior', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(33, 'Albert Maximilian', 'design.brunosantos@gmail.com', '1234', '2', '', 'Albert Maximilian', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(34, 'Alberto DIniz', 'adinizfilho@gmail.com', '1234', '2', '', 'Alberto Diniz Filho', 0, 15, 'Fevereiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(35, 'Ademar NemÃ©sio', 'design.brunosantos@gmail.com', '1234', '2', '', 'Ademar NemÃ©sio B. V.', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, '(26)'),
(36, 'Alexandre Barbosa', 'design.brunosantos@gmail.com', '1234', '2', '', 'Alexandre Barbosa', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(37, 'Amilcar Nogueira', 'design.brunosantos@gmail.com', '1234', '2', '', 'Amilcar Nogueira', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(38, 'Ana Rosa Figueiredo', 'design.brunosantos@gmail.com', '1234', '2', '', 'Ana Rosa P. Figueiredo', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(39, 'AndrÃ© Aguiar', 'andreaguiaroftalmo@gmail.com', '1234', '2', '', 'AndrÃ© Aguiar de Oliveira', 0, 14, 'Setembro', 0, '0000-00-00', '', 4, 0, 0, ''),
(40, 'AndrÃ© Andrade', 'andreoandrade@task.com.br', '1234', '2', '', 'AndrÃ© Oliveira Andrade', 0, 19, 'Dezembro', 0, '0000-00-00', '', 4, 0, 0, ''),
(41, 'AndrÃ© Cardoso Nery', 'andrecnery@yahoo.com.br', '1234', '2', '', 'AndrÃ© Cardoso Nery', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(42, 'AndrÃ© Vasconcellos', 'design.brunosantos@gmail.com', '1234', '2', '', 'AndrÃ© Vasconcellos', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(43, 'AntÃ´nio Carlos', 'ankarlusha@gmail.com', '1234', '2', '', 'AntÃ´nio Carlos Lopes Chaves', 0, 1, 'Fevereiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(44, 'AntÃ´nio Sansoni', 'design.brunosantos@gmail.com', '1234', '2', '', 'AntÃ´nio Sansoni', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(45, 'BetÃ¢nia AraÃºjo', 'betaniabh@gmail.com', '1234', '2', '', 'BetÃ¢nia Maria Pontes AraÃºjo', 0, 27, 'Fevereiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(46, 'Breno Lino', 'pectenoculi@hotmail.com', '1234', '2', '', 'Breno Teixeira Lino', 0, 27, 'Maio', 0, '0000-00-00', '', 4, 0, 0, ''),
(47, 'Caio MÃ¡rcio', 'design.brunosantos@gmail.com', '1234', '2', '', 'Caio MÃ¡rcio', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(48, 'Carlos Flister', 'design.brunosantos@gmail.com', '1234', '2', '', 'Carlos E. R. Flister', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(49, 'Carlos Eduardo Velos', 'cerveloso@hotmail.com', '1234', '2', '', 'Carlos Eduardo dos Reis Veloso', 0, 12, 'Abril', 0, '0000-00-00', '', 4, 0, 0, ''),
(50, 'Carlos Mafra', 'design.brunosantos@gmail.com', '1234', '2', '', 'Carlos Mafra', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(51, 'CÃ©sar Batistela', 'design.brunosantos@gmail.com', '1234', '2', '', 'CÃ©sar Batistela', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(52, 'Christian Marcellus', 'design.brunosantos@gmail.com', '1234', '2', '', 'Christian Marcellus', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(53, 'Christiano Fausto', 'design.brunosantos@gmail.com', '1234', '2', '', 'Christiano Fausto', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(54, 'Cinyra QuintÃ£o', 'cinyrabelem@hotmail.com', '1234', '2', '', 'Cinyra QuintÃ£o Silva BelÃ©m', 0, 30, 'Novembro', 0, '0000-00-00', '', 4, 0, 0, ''),
(55, 'Cristiane Samara', 'design.brunosantos@gmail.com', '1234', '2', '', 'Cristiane Samara Bot', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(56, 'Daise Meira', 'design.brunosantos@gmail.com', '1234', '2', '', 'Daise M. Meira', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(57, 'Daniel Vasconcelo', 'design.brunosantos@gmail.com', '1234', '2', '', 'Daniel V. Vasconcelo', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(58, 'DÃ©bora Pinheiro', 'design.brunosantos@gmail.com', '1234', '2', '', 'DÃ©bora Pinheiro', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(59, 'DÃ©cio da Costa', 'design.brunosantos@gmail.com', '1234', '2', '', 'DÃ©cio da Costa', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(60, 'Diogo Bezerra', 'design.brunosantos@gmail.com', '1234', '2', '', 'Diogo Bezerra', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(61, 'Edmar Chartone', 'edmarchartone@terra.com.br', '1234', '2', '', 'Edmar Chartone de Souza Filho', 0, 31, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(62, 'Edmundo AmÃ©rico', 'edmundo@bholhos.com.br', '1234', '2', '', 'Edmundo AmÃ©rico Dias Soares', 0, 4, 'Julho', 0, '0000-00-00', '', 4, 0, 0, ''),
(63, 'Eduardo Adan Franca', 'design.brunosantos@gmail.com', '1234', '2', '', 'Eduardo Adan Franca', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(64, 'Eduardo Milhomens', 'eduardomilhomens@imol.com.br', '1234', '2', '', 'Eduardo Gutemberg Moreira Milhomens', 1, 14, 'Novembro', 0, '0000-00-00', '', 4, 0, 0, ''),
(65, 'Eliane Lamonier', 'design.brunosantos@gmail.com', '1234', '2', '', 'Eliane Lamonier', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(66, 'Elke Passos', 'elkepassos@hotmail.com', '1234', '2', '', 'Elke Passos', 1, 28, 'Fevereiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(67, 'EmÃ­lia Colosimo', 'design.brunosantos@gmail.com', '1234', '2', '', 'EmÃ­lia Colosimo', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(68, 'Fernando Pedrosa', 'design.brunosantos@gmail.com', '1234', '2', '', 'Fernando Pedrosa', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(69, 'Fernando Sanabi', 'design.brunosantos@gmail.com', '1234', '2', '', 'Fernando Sanabi', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(70, 'FlÃ¡via BelÃ©m', 'flaviabelem@hotmail.com', '1234', '2', '', 'FlÃ¡via QuintÃ£o da Silva BelÃ©m', 1, 23, 'Maio', 0, '0000-00-00', '', 4, 0, 0, ''),
(71, 'FlÃ¡vio Marigo', 'flavio.marigo@gmail.com', '1234', '2', '', 'FlÃ¡vio de Andrade Marigo', 1, 22, 'Abril', 0, '0000-00-00', '', 4, 0, 0, ''),
(72, 'FlÃ¡vio Tepedino', 'flaviot@gmail.com', '1234', '2', '', 'FlÃ¡vio Tepedino Aguiar de Oliveira', 1, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(73, 'Francisco Nery', 'francisconery@hotmail.com', '1234', '2', '', 'Francisco Nery Junior', 1, 28, 'Junho', 0, '0000-00-00', '', 4, 0, 0, ''),
(74, 'Frederico Bicalho', 'design.brunosantos@gmail.com', '1234', '2', '', 'Frederico Bicalho', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(75, 'Frederico Braga', 'design.brunosantos@gmail.com', '1234', '2', '', 'Frederico Braga', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(76, 'Frederico Pereira', 'fpereira@task.com.br', '1234', '2', '', 'Frederico Augusto Souza Pereira', 1, 10, 'Setembro', 0, '0000-00-00', '', 4, 0, 0, ''),
(77, 'Gilberto Sanches', 'design.brunosantos@gmail.com', '1234', '2', '', 'Gilberto Sanches', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(78, 'Gustavo Capanema', 'design.brunosantos@gmail.com', '1234', '2', '', 'Gustavo Capanema', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(79, 'Hugo Leonardo', 'design.brunosantos@gmail.com', '1234', '2', '', 'Hugo Leonardo de Mag', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(80, 'Igor Fratezzi', 'igorf@task.com.br', '1234', '2', '', 'Igor Ribeiro Fratezzi GonÃ§alves', 1, 27, 'Agosto', 0, '0000-00-00', '', 4, 0, 0, ''),
(81, 'Indramara Melo', 'design.brunosantos@gmail.com', '1234', '2', '', 'Indramara Melo', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(82, 'Isabela Savassi', 'design.brunosantos@gmail.com', '1234', '2', '', 'Isabela Savassi', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(83, 'Isabela Soares', 'design.brunosantos@gmail.com', '1234', '2', '', 'Isabela Soares', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(84, 'Ivan Borges', 'design.brunosantos@gmail.com', '1234', '2', '', 'Ivan Borges', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(85, 'Ivan Thomas', 'ivanlarge@hotmail.com', '1234', '2', '', 'Ivan Thomas Large', 1, 3, 'MarÃ§o', 0, '0000-00-00', '', 4, 0, 0, ''),
(86, 'Jacqueline Katina', 'dra.jacquelinekatina@yahoo.com.br', '1234', '2', '', 'Jacqueline Hedva Katina', 1, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(87, 'JoÃ£o Angelo Miranda', 'design.brunosantos@gmail.com', '1234', '2', '', 'JoÃ£o Angelo Miranda', 1, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(88, 'JoÃ£o Daniel', 'design.brunosantos@gmail.com', '1234', '2', '', 'JoÃ£o Daniel', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(89, 'Joel Boteon', 'botteon@medicina.ufmg.br', '1234', '2', '', 'Joel Edmur Boteon', 1, 19, 'Maio', 0, '0000-00-00', '', 4, 0, 0, ''),
(90, 'JosÃ© AloÃ­sio Dias', 'design.brunosantos@gmail.com', '1234', '2', '', 'JosÃ© AloÃ­sio Dias Ma', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(91, 'JosÃ© Roberto', 'design.brunosantos@gmail.com', '1234', '2', '', 'JosÃ© Roberto', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(92, 'JÃºlia Bicharra', 'design.brunosantos@gmail.com', '1234', '2', '', 'JÃºlia Bicharra', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(93, 'Juliana QuintÃ£o', 'design.brunosantos@gmail.com', '1234', '2', '', 'Juliana QuintÃ£o Mend', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(94, 'Juliana Salles', 'julianaaroeira@hotmail.com', '1234', '2', '', 'Juliana Aroeira Salles', 1, 6, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(95, 'Juliano Souza Maia', 'design.brunosantos@gmail.com', '1234', '2', '', 'Juliano Souza Maia', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(96, 'Karen Ramalho', 'design.brunosantos@gmail.com', '1234', '2', '', 'Karen R. B. Ramalho', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(97, 'Laura Coelho', 'design.brunosantos@gmail.com', '1234', '2', '', 'Laura EmÃ­lia N. Coelho', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(98, 'Leonardo Diniz', 'design.brunosantos@gmail.com', '1234', '2', '', 'Leonardo Diniz', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(99, 'Leonardo Dolabela', 'design.brunosantos@gmail.com', '1234', '2', '', 'Leonardo Dolabela', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(100, 'Lilian Grace', 'design.brunosantos@gmail.com', '1234', '2', '', 'Lilian Grace', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(101, 'Luciana Alkmim', 'design.brunosantos@gmail.com', '1234', '2', '', 'Luciana Alkmim', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(102, 'Luciana Mares', 'design.brunosantos@gmail.com', '1234', '2', '', 'Luciana Mares', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(103, 'Marcelo Miranda', 'design.brunosantos@gmail.com', '1234', '2', '', 'Marcelo C. Miranda', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(104, 'Marcelo Mendes ', 'design.brunosantos@gmail.com', '1234', '2', '', 'Marcelo Mendes de Fr', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(105, 'Marcelo Pereira', 'marceloanest@gmail.com', '1234', '2', '', 'Marcelo de Oliveira Pereira', 1, 25, 'Setembro', 0, '0000-00-00', '', 4, 0, 0, ''),
(106, 'Marco Tanure', 'magt99@yahoo.com', '1234', '2', '', 'Marco AntÃ´nio Guarino Tanure', 1, 17, 'MarÃ§o', 0, '0000-00-00', '', 4, 0, 0, ''),
(107, 'Maria de Lourdes ', 'design.brunosantos@gmail.com', '1234', '2', '', 'Maria de Lourdes Res', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(108, 'Maria EmÃ­lia Darwich', 'design.brunosantos@gmail.com', '1234', '2', '', 'Maria EmÃ­lia Darwich', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(109, 'Maria Frasson', 'frasson@task.com.br', '1234', '2', '', 'Maria da ConceiÃ§Ã£o Frasson', 1, 3, 'Agosto', 0, '0000-00-00', '', 4, 0, 0, ''),
(110, 'Maria JosÃ© Calixto', 'clinicacalixto@gmail.com.br', '1234', '2', '', 'Maria JosÃ© Calixto Rodrigues ', 1, 16, 'Dezembro', 0, '0000-00-00', '', 4, 0, 0, ''),
(111, 'Maria ValÃ©ria', 'mvcpereira@hotmail.com', '1234', '2', '', 'Maria ValÃ©ria Correia Pereira', 1, 5, 'Outubro', 0, '0000-00-00', '', 4, 0, 0, ''),
(112, 'Mario Carlos Alves', 'design.brunosantos@gmail.com', '1234', '2', '', 'Mario Carlos Alves', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(113, 'Miguel Gontijo', 'miguelgontijo@gmail.com', '1234', '2', '', 'Miguel Ã‚ngelo Gontijo Alvares', 1, 3, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(114, 'Nassim JÃºnior', 'clinicacalixto@veloxmail.com.br', '1234', '2', '', 'Nassim da Silveira Calixto JÃºnior', 1, 13, 'Novembro', 0, '0000-00-00', '', 4, 0, 0, ''),
(115, 'Newton Vitral', 'npvitral@oi.com.br', '1234', '2', '', 'Newton Pena Vitral', 1, 9, 'Fevereiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(116, 'Odair GuimarÃ£es', 'design.brunosantos@gmail.com', '1234', '2', '', 'Odair GuimarÃ£es', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(117, 'PatrÃ­cia Dias', 'design.brunosantos@gmail.com', '1234', '2', '', 'PatrÃ­cia Dias', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(118, 'PatrÃ­cia Marigo', 'iglaucoma@terra.com.br', '1234', '2', '', 'PatrÃ­cia Vianna BrandÃ£o Marigo', 1, 1, 'Fevereiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(119, 'Roberto Abdalla', 'design.brunosantos@gmail.com', '1234', '2', '', 'Roberto Abdalla', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(120, 'Roberto Santos', 'design.brunosantos@gmail.com', '1234', '2', '', 'Roberto M. O. Santos', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(121, 'Rodrigo Ferreira', 'design.brunosantos@gmail.com', '1234', '2', '', 'Rodrigo Ferreira De', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(122, 'Rodrigo Versiani', 'design.brunosantos@gmail.com', '1234', '2', '', 'Rodrigo Versiani', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(123, 'RogÃ©rio Tarcia', 'almeidatarcia@yahoo.com.br', '1234', '2', '', 'RogÃ©rio de Almeida Tarcia', 1, 25, 'Novembro', 0, '0000-00-00', '', 4, 0, 0, ''),
(124, 'Ronaldo Badaro', 'design.brunosantos@gmail.com', '1234', '2', '', 'Ronaldo Badaro', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(125, 'Ronei Maia', 'roneimaia@yahoo.com.br', '1234', '2', '', 'Ronei de Souza Maia', 1, 29, 'Maio', 0, '0000-00-00', '', 4, 0, 0, ''),
(126, 'Rubens GonÃ§alves', 'design.brunosantos@gmail.com', '1234', '2', '', 'Rubens GonÃ§alves', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(127, 'Samuel Oliveira', 'design.brunosantos@gmail.com', '1234', '2', '', 'Samuel P. D. Oliveira', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(128, 'Saulo Watanabe', 'saulowat@gmail.com', '1234', '2', '', 'Saulo Watanabe', 1, 9, 'Abril', 0, '0000-00-00', '', 4, 0, 0, ''),
(129, 'SebastiÃ£o Cronemberg', 'secronem@gmail.com', '1234', '2', '', 'SebastiÃ£o Cronemberg', 1, 26, 'Maio', 0, '0000-00-00', '', 4, 0, 0, ''),
(130, 'Thiago Milhomens', 'thiagomilhomens@hotmail.com', '1234', '2', '', 'Thiago Gutemberg Avellar Milhomens', 1, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(131, 'VirgÃ­nia Mares', 'design.brunosantos@gmail.com', '1234', '2', '', 'VirgÃ­nia S. L. Mares', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(132, 'TÃºlio Hannas', 'design.brunosantos@gmail.com', '1234', '2', '', 'TÃºlio Reis Hannas', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(133, 'Tiago Carvalho', 'design.brunosantos@gmail.com', '1234', '2', '', 'Tiago Carvalho', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(134, 'Taliana Freitas', 'design.brunosantos@gmail.com', '1234', '2', '', 'Taliana Freitas', 0, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(135, 'Eduardo Antunes', 'eantunesneto@yahoo.com.br', '1234', '2', '', 'Eduardo Antunes Neto', 1, 1, 'Janeiro', 0, '0000-00-00', '', 4, 0, 0, ''),
(136, 'Elanilze Natividade', 'elanilze@gmail.com', '1234', '2', '', 'Elanilze Natividade Costa', 1, 16, 'Junho', 0, '0000-00-00', '', 4, 0, 0, ''),
(137, 'Ana Paula Natividade', 'gerentedeoperacoes@nucleomg.com.br', '1234', '7', '', 'Ana Paula Natividade', 1, 29, 'Agosto', 0, '0000-00-00', '', 1, 0, 0, ''),
(138, 'Warlen A. Nobre', 'warlencnobre@yahoo.com.br', '1234', '7', '', 'Warlen A. Nobre', 1, 1, 'Junho', 0, '0000-00-00', '', 1, 0, 0, ''),
(139, 'ClÃ¡udia da C Ferreira', 'milanezclaudia785@gmail.com', '1234', '7', '', 'ClÃ¡udia da C Ferreira', 1, 26, 'Maio', 0, '0000-00-00', '', 8, 0, 0, ''),
(140, 'JoÃ£o Cordeiro', 'joaocs@bol.com.br', '1234', '7', '', 'JoÃ£o Cordeiro', 1, 27, 'Junho', 0, '0000-00-00', '', 8, 0, 0, ''),
(141, 'Jacques Ramos Houly', ' contato@webcafeconsultoria.com.br', '1234', '7', '', 'Jacques Ramos Houly', 1, 28, 'Fevereiro', 0, '0000-00-00', '', 9, 0, 0, ''),
(142, 'RaÃ­ssa Desiree', 'raissadmr@yahoo.com.br', '1234', '7', '', 'RaÃ­ssa Desiree', 1, 26, 'Dezembro', 0, '0000-00-00', '', 10, 0, 0, ''),
(143, 'Aliny GonÃ§alves de S', 'contato@webcafeconsultoria.com.br', '1234', '7', '', 'Aliny GonÃ§alves de S', 1, 3, 'Outubro', 0, '0000-00-00', '', 11, 0, 0, ''),
(144, 'Marcela Silva Ramos', 'contato@webcafeconsultoria.com.br', '1234', '7', '', 'Marcela Silva Ramos', 1, 16, 'Julho', 0, '0000-00-00', '', 11, 0, 0, ''),
(145, 'Marcilene Neves', 'contato@webcafeconsultoria.com.br', '1234', '7', '', 'Marcilene Neves', 1, 23, 'Agosto', 0, '0000-00-00', '', 11, 0, 0, ''),
(146, 'Poliana Mara', 'contato@webcafeconsultoria.com.br', '1234', '7', '', 'Poliana Mara', 1, 28, 'Fevereiro', 0, '0000-00-00', '', 11, 0, 0, ''),
(147, 'Rafaela S Marriel', 'contato@webcafeconsultoria.com.br', '1234', '7', '', 'Rafaela S Marriel', 1, 9, 'Julho', 0, '0000-00-00', '', 11, 0, 0, ''),
(148, 'Adriana S. Correa', 'contato@webcafeconsultoria.com.br', '1234', '7', '', 'Adriana S. Correa', 1, 1, 'Janeiro', 0, '0000-00-00', '', 12, 0, 0, ''),
(149, 'Ana Claudia Rocha', 'contato@webcafeconsultoria.com.br', '1234', '7', '', 'Ana Claudia Rocha', 1, 20, 'Julho', 0, '0000-00-00', '', 12, 0, 0, ''),
(150, 'Camila C. Ponzo', 'contato@webcafeconsultoria.com.br', '1234', '7', '', 'Camila C. Ponzo', 1, 1, 'Janeiro', 0, '0000-00-00', '', 12, 0, 0, ''),
(151, 'Cathia Genoveva', 'contato@webcafeconsultoria.com.br', '1234', '7', '', 'Cathia Genoveva', 1, 17, 'Junho', 0, '0000-00-00', '', 12, 0, 0, ''),
(152, 'Cleonice', 'contato@webcafeconsultoria.com.br', '1234', '7', '', 'Cleonice', 1, 11, 'MarÃ§o', 0, '0000-00-00', '', 12, 0, 0, ''),
(153, 'Erika Batista', 'contato@webcafeconsultoria.com.br', '1234', '7', '', 'Erika Batista', 1, 28, 'Maio', 0, '0000-00-00', '', 12, 0, 0, ''),
(154, 'Mislene A. S. Santos', 'contato@webcafeconsultoria.com.br', '1234', '7', '', 'Mislene A. S. Santos', 1, 5, 'Junho', 0, '0000-00-00', '', 12, 0, 0, ''),
(155, 'Priscila T. A. Neves', 'con', '1234', '7', '', 'Priscila T. A. Neves', 1, 8, 'Maio', 0, '0000-00-00', '', 12, 0, 0, ''),
(156, 'Rafaela De J Veloso', 'contato@webcafeconsultoria.com.br', '1234', '7', '', 'Rafaela De J Veloso', 1, 21, 'Setembro', 0, '0000-00-00', '', 12, 0, 0, ''),
(157, 'Terezinha I C Souza', 'contato@webcafeconsultoria.com.br', '1234', '7', '', 'Terezinha I C Souza', 1, 4, 'Outubro', 0, '0000-00-00', '', 12, 0, 0, ''),
(158, 'Williana L. Vidal', 'contato@webcafeconsultoria.com.br', '1234', '7', '', 'Williana L. Vidal', 1, 29, 'Abril', 0, '0000-00-00', '', 12, 0, 0, ''),
(159, 'Alex J J Alomba', 'contato@webcafeconsultoria.com.br', '1234', '7', '', 'Alex J J Alomba', 1, 10, 'Dezembro', 0, '0000-00-00', '', 13, 0, 0, ''),
(160, 'Josiany S. Amaral', 'josianyamaral@hotmail.com', '1234', '7', '', 'Josiany S. Amaral	', 1, 4, 'Dezembro', 0, '0000-00-00', '', 13, 0, 0, ''),
(161, 'Larissa G F Da Silva', 'contato@webcafeconsultoria.com.br', '1234', '7', '', 'Larissa G F Da Silva', 1, 2, 'MarÃ§o', 0, '0000-00-00', '', 13, 0, 0, ''),
(162, 'Marcos Felipe Mendes', 'con', '1234', '7', '', 'Marcos Felipe Mendes', 1, 7, 'Dezembro', 0, '0000-00-00', '', 13, 0, 0, ''),
(163, 'Sarah M G Nunes', 'contato@webcafeconsultoria.com.br', '1234', '7', '', 'Sarah M G Nunes', 1, 2, 'Outubro', 0, '0000-00-00', '', 13, 0, 0, ''),
(164, 'Adriana P Rossi', 'adriana.drirossi@yahoo.com.br', '1234', '7', '', 'Adriana P Rossi', 1, 10, 'Dezembro', 0, '0000-00-00', '', 14, 0, 0, '(26)'),
(165, 'RosÃ¢nia C Bertoldo', 'rosania.bertoldo@gmail.com', '1234', '7', '', 'RosÃ¢nia C Bertoldo', 0, 9, 'Setembro', 0, '0000-00-00', '', 2, 0, 0, ''),
(166, 'Tatiana E. F. Santos', 'tatiana_emanuelle@hotmail.com', '1234', '7', '', 'Tatiana E. F. Santos', 1, 28, 'Junho', 0, '0000-00-00', '', 14, 0, 0, ''),
(167, 'Tatiana Santos', 'tatiposantos@yahoo.com.br', '1234', '7', '', 'Tatiana Santos', 1, 19, 'Novembro', 0, '0000-00-00', '', 14, 0, 0, ''),
(168, 'Heliane R. Aquino', 'heliane@hotmail.com', '1234', '7', '', 'Heliane R. Aquino', 1, 20, 'Abril', 0, '0000-00-00', '', 15, 0, 0, ''),
(169, 'Marilene das G A S', 'silvamarilene100@gmail.com', '1234', '7', '', 'Marilene das G A S', 1, 31, 'Maio', 0, '0000-00-00', '', 15, 0, 0, ''),
(170, 'Romilda V de Sales', 'contato@webcafeconsultoria.com.br', '1234', '7', '', 'Romilda V de Sales', 1, 27, 'Julho', 0, '0000-00-00', '', 15, 0, 0, ''),
(171, 'Adrielly . F. Santos', 'driellylimex@hotmail.com', '1234', '7', '', 'Adrielly . F. Santos', 1, 29, 'Outubro', 0, '0000-00-00', '', 16, 0, 0, ''),
(172, 'Ana Carolina F Silva', 'anacarolinafracadasilva@gmail.com', '1234', '7', '', 'Ana Carolina F Silva', 1, 29, 'Dezembro', 0, '0000-00-00', '', 16, 0, 0, ''),
(173, 'Daiane Aparecida', 'contato@webcafeconsultoria.com.br', '1234', '7', '', 'Daiane Aparecida', 1, 5, 'Abril', 0, '0000-00-00', '', 16, 0, 0, ''),
(174, 'Keila Joana Da Silva', 'keilaj2@hotmail.com', '1234', '7', '', 'Keila Joana Da Silva', 1, 24, 'Fevereiro', 0, '0000-00-00', '', 16, 0, 0, ''),
(175, 'Kenneth R DurÃ£es', 'kennethduraes@hotmail.com', '1234', '7', '', 'Kenneth R DurÃ£es', 1, 13, 'Maio', 0, '0000-00-00', '', 16, 0, 0, ''),
(176, 'Marluce M. M. Aranha', 'contato@webcafeconsultoria.com.br', '1234', '7', '', 'Marluce M. M. Aranha', 1, 29, 'Dezembro', 0, '0000-00-00', '', 16, 0, 0, ''),
(177, 'Sheilla Costa Silva', 'contato@webcafeconsultoria.com.br', '1234', '7', '', 'Sheilla Costa Silva', 1, 15, 'Setembro', 0, '0000-00-00', '', 16, 0, 0, ''),
(178, 'Suellen C. Costa', 'suellencristinecosta@hotmail.com', '1234', '7', '', 'Suellen C. Costa', 1, 16, 'Abril', 0, '0000-00-00', '', 16, 0, 0, ''),
(179, 'Regiane A. de S. GodÃ³i', 'reagodoi@hotmail.com', '1234', '7', '', 'Regiane A. de S. GodÃ³i', 1, 19, 'Dezembro', 0, '0000-00-00', '', 18, 0, 0, ''),
(180, 'Adriana R. C. Silva', 'adribiza@gmail.com', '1234', '7', '', 'Adriana R. C. Silva', 1, 22, 'Junho', 0, '0000-00-00', '', 19, 0, 0, ''),
(181, 'Ana Carolina P. Peres', 'aninhacaroli@yahoo.com.br', '1234', '7', '', 'Ana Carolina P. Peres', 1, 11, 'Julho', 0, '0000-00-00', '', 19, 0, 0, ''),
(182, 'Bruna S GuimarÃ£es', 'brunasoaresa@hotmail.com', '1234', '7', '', 'Bruna S GuimarÃ£es', 1, 1, 'Fevereiro', 0, '0000-00-00', '', 19, 0, 0, ''),
(183, 'Bruna T. Carmo', 'bruna_tamires1107@yahoo.com.br', '1234', '7', '', 'Bruna T. Carmo', 1, 25, 'Abril', 0, '0000-00-00', '', 19, 0, 0, ''),
(184, 'Claudia M. Giarola', 'claudiagiarola@yahoo.com.br', '1234', '7', '', 'Claudia M. Giarola', 1, 10, 'Setembro', 0, '0000-00-00', '', 19, 0, 0, ''),
(185, 'ClaudinÃ©ia A Rodrigues', 'claudineiardg@gmail.com', '1234', '7', '', 'ClaudinÃ©ia A Rodrigues', 1, 24, 'Maio', 0, '0000-00-00', '', 19, 0, 0, ''),
(186, 'ClÃ­cia Leandro Da Si', 'contato@webcafeconsultoria.com.br', '1234', '7', '', 'ClÃ­cia Leandro Da Si', 1, 15, 'Fevereiro', 0, '0000-00-00', '', 19, 0, 0, ''),
(187, 'Denise F. de Oliveir', 'dfrancisca@oi.com.br', '1234', '7', '', 'Denise F. de Oliveir', 1, 26, 'Abril', 0, '0000-00-00', '', 19, 0, 0, ''),
(188, 'Etyene C S Moyle', 'etyenemoyle@hotmail.com', '1234', '7', '', 'Etyene C S Moyle', 1, 8, 'Julho', 0, '0000-00-00', '', 19, 0, 0, ''),
(189, 'Kezia Cleide Silva', 'keziacs@yahoo.com.br', '1234', '7', '', 'Kezia Cleide Silva', 1, 19, 'Fevereiro', 0, '0000-00-00', '', 19, 0, 0, ''),
(190, 'Laudiane P G Da Silva', 'laugenelhu@hotmail.com', '1234', '7', '', 'Laudiane P G Da Silva', 1, 7, 'Abril', 0, '0000-00-00', '', 19, 0, 0, ''),
(191, 'Lorena M GonÃ§alves', 'lmgeventolmg@gmail.com', '1234', '7', '', 'Lorena M GonÃ§alves', 1, 5, 'Janeiro', 0, '0000-00-00', '', 19, 0, 0, ''),
(192, 'PatrÃ­cia R Goncalves', 'patriciag_rocha@hotmail.com', '1234', '7', '', 'PatrÃ­cia R Goncalves', 1, 3, 'Agosto', 0, '0000-00-00', '', 19, 0, 0, ''),
(193, 'Poliana A da Mata P', 'polianaanacleto2@gmail.com', '1234', '7', '', 'Poliana A da Mata P', 1, 19, 'Setembro', 0, '0000-00-00', '', 19, 0, 0, ''),
(194, 'Valdineia L G Rodrigues', 'valdineialopesgoncalves@yahoo.com.br', '1234', '7', '', 'Valdineia L G Rodrigues', 1, 1, 'Janeiro', 0, '0000-00-00', '', 19, 0, 0, ''),
(195, 'Mariana S GonÃ§alves', 'marimariana.goncalves@gmail.com', '1234', '7', '', 'Mariana S GonÃ§alves', 1, 12, 'Abril', 0, '0000-00-00', '', 20, 0, 0, ''),
(196, 'Jaqueline M. B. Mach', 'jaquelinembm@yahoo.com.br', '1234', '7', '', 'Jaqueline M. B. Mach', 1, 4, 'MarÃ§o', 0, '0000-00-00', '', 21, 0, 0, ''),
(197, 'Natalia F Mesquita', 'mesquitatour@ibest.com.br', '1234', '7', '', 'Natalia F Mesquita', 1, 19, 'Dezembro', 0, '0000-00-00', '', 21, 0, 0, ''),
(198, '', 'adelsonfernandes@yahoo.com.br', '1234', '7', '', 'Adelson Fernandes Faria', 1, 14, 'Janeiro', 0, '0000-00-00', '', 22, 0, 0, '(26)'),
(199, '', 'wellingtondfv@gmail.com', '1234', '1', '', 'Wellington F Vieira', 1, 4, 'Outubro', 0, '0000-00-00', '', 22, 26, 0, ''),
(200, 'Wesley P C Ferreira', 'wesley_parreiras@yahoo.com.br', '1234', '7', '', 'Wesley P C Ferreira', 1, 23, 'MarÃ§o', 0, '0000-00-00', '', 22, 0, 0, ''),
(201, 'Bruno Santos', 'bruno.jabo@gmail.com', '1234', '7', '', 'Bruno CÃ©sar Soares Santos', 0, 1, 'Dezembro', 0, '0000-00-00', '', 26, 26, 0, '(26)'),
(203, '', '', '', '', '', '', 0, 0, '', 0, '0000-00-00', '', 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `vagas`
--

CREATE TABLE `vagas` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `categoria` int(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `data` date NOT NULL,
  `tipo` varchar(20) NOT NULL,
  `endereco` varchar(200) NOT NULL,
  `tel01` varchar(50) NOT NULL,
  `tel02` varchar(50) NOT NULL,
  `tel03` varchar(50) NOT NULL,
  `texto` text NOT NULL,
  `qte` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `vagas`
--

INSERT INTO `vagas` (`id`, `nome`, `categoria`, `email`, `data`, `tipo`, `endereco`, `tel01`, `tel02`, `tel03`, `texto`, `qte`) VALUES
(31, 'nome', 0, 'email@email.com', '2016-12-18', '', 'endereco', 't01', 't02', 't03', 'asdf', 1),
(32, 'nome2', 0, 'email@email.com2', '2016-12-18', '', 'endereco2', 't012', 't022', 't032', 'asdf2', 12),
(34, '', 0, '', '0000-00-00', '', '', '', '', '', '', 0);

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `artigos`
--
ALTER TABLE `artigos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `atividades`
--
ALTER TABLE `atividades`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `avisos`
--
ALTER TABLE `avisos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `beneficios`
--
ALTER TABLE `beneficios`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `busca`
--
ALTER TABLE `busca`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `carrinho`
--
ALTER TABLE `carrinho`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `chamados`
--
ALTER TABLE `chamados`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `controle`
--
ALTER TABLE `controle`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `dados`
--
ALTER TABLE `dados`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_2` (`id`),
  ADD KEY `id_3` (`id`);

--
-- Índices de tabela `docs_enviados`
--
ALTER TABLE `docs_enviados`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `documentacao_fixa`
--
ALTER TABLE `documentacao_fixa`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `documentos`
--
ALTER TABLE `documentos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `doc_associado`
--
ALTER TABLE `doc_associado`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `doc_financeiro`
--
ALTER TABLE `doc_financeiro`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `financeiros`
--
ALTER TABLE `financeiros`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `grupos`
--
ALTER TABLE `grupos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `historico`
--
ALTER TABLE `historico`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `iframes`
--
ALTER TABLE `iframes`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `imagens`
--
ALTER TABLE `imagens`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `lista`
--
ALTER TABLE `lista`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `locacao`
--
ALTER TABLE `locacao`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `paginas`
--
ALTER TABLE `paginas`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `papelaria`
--
ALTER TABLE `papelaria`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `pergunta`
--
ALTER TABLE `pergunta`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `praticas`
--
ALTER TABLE `praticas`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `processos`
--
ALTER TABLE `processos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `ramais`
--
ALTER TABLE `ramais`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `servicos`
--
ALTER TABLE `servicos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `sessao`
--
ALTER TABLE `sessao`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Índices de tabela `setores`
--
ALTER TABLE `setores`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `solicitacoes`
--
ALTER TABLE `solicitacoes`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `textos`
--
ALTER TABLE `textos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `tipo_de_usuario`
--
ALTER TABLE `tipo_de_usuario`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `usuarioz`
--
ALTER TABLE `usuarioz`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `vagas`
--
ALTER TABLE `vagas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_2` (`id`),
  ADD KEY `id_3` (`id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `artigos`
--
ALTER TABLE `artigos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de tabela `atividades`
--
ALTER TABLE `atividades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT de tabela `avisos`
--
ALTER TABLE `avisos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de tabela `beneficios`
--
ALTER TABLE `beneficios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de tabela `busca`
--
ALTER TABLE `busca`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `carrinho`
--
ALTER TABLE `carrinho`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `chamados`
--
ALTER TABLE `chamados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de tabela `controle`
--
ALTER TABLE `controle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT de tabela `dados`
--
ALTER TABLE `dados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `docs_enviados`
--
ALTER TABLE `docs_enviados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de tabela `documentacao_fixa`
--
ALTER TABLE `documentacao_fixa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de tabela `documentos`
--
ALTER TABLE `documentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT de tabela `doc_associado`
--
ALTER TABLE `doc_associado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- AUTO_INCREMENT de tabela `doc_financeiro`
--
ALTER TABLE `doc_financeiro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- AUTO_INCREMENT de tabela `financeiros`
--
ALTER TABLE `financeiros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `grupos`
--
ALTER TABLE `grupos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `historico`
--
ALTER TABLE `historico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de tabela `iframes`
--
ALTER TABLE `iframes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `imagens`
--
ALTER TABLE `imagens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT de tabela `item`
--
ALTER TABLE `item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de tabela `lista`
--
ALTER TABLE `lista`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de tabela `locacao`
--
ALTER TABLE `locacao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `options`
--
ALTER TABLE `options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `paginas`
--
ALTER TABLE `paginas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de tabela `papelaria`
--
ALTER TABLE `papelaria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de tabela `pergunta`
--
ALTER TABLE `pergunta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de tabela `praticas`
--
ALTER TABLE `praticas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT de tabela `processos`
--
ALTER TABLE `processos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `produtos`
--
ALTER TABLE `produtos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT de tabela `ramais`
--
ALTER TABLE `ramais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `servicos`
--
ALTER TABLE `servicos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de tabela `setores`
--
ALTER TABLE `setores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de tabela `slides`
--
ALTER TABLE `slides`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de tabela `solicitacoes`
--
ALTER TABLE `solicitacoes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de tabela `textos`
--
ALTER TABLE `textos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `tipo_de_usuario`
--
ALTER TABLE `tipo_de_usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de tabela `usuarioz`
--
ALTER TABLE `usuarioz`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=204;

--
-- AUTO_INCREMENT de tabela `vagas`
--
ALTER TABLE `vagas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
